import org.gradle.internal.os.OperatingSystem

plugins {
    java
    kotlin("jvm") version "1.3.11"
}

group = "tomasvolker"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()

    maven { setUrl("http://dl.bintray.com/tomasvolker/maven") }
    maven { url = uri("http://dl.bintray.com/kyonifer/maven") }
    maven { url = uri("https://dl.bintray.com/openrndr/openrndr/") }
    maven { url = uri("https://jitpack.io") }
}

val openrndrVersion = "0.3.32"

val openrndrOS = when (OperatingSystem.current()) {
    OperatingSystem.WINDOWS -> "windows"
    OperatingSystem.LINUX -> "linux-x64"
    OperatingSystem.MAC_OS -> "macos"
    else -> error("unsupported OS")
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib")
    
    implementation(group = "tomasvolker", name = "numeriko-core", version = "0.0.3")
    implementation(group = "tomasvolker", name = "kyplot", version = "0.0.1")

    compile("org.nield:kotlin-statistics:1.1.3")

    runtime("org.openrndr:openrndr-gl3:$openrndrVersion")
    runtime("org.openrndr:openrndr-gl3-natives-$openrndrOS:$openrndrVersion")
    compile("org.openrndr:openrndr-core:$openrndrVersion")
    compile("org.openrndr:openrndr-extensions:$openrndrVersion")
    compile("com.github.openrndr.orx:orx-camera:v0.0.20")

    implementation(group = "com.kyonifer", name = "koma-core-ejml", version = "0.12")

    implementation(group = "com.github.tomasvolker", name = "parallel-utils", version = "v1.0")

    implementation("com.github.TomasVolker:koofcv:develop-SNAPSHOT")
    implementation("com.github.TomasVolker:openrndr-math:master-SNAPSHOT")
    
    testImplementation(group = "junit", name = "junit", version = "4.12")

}
