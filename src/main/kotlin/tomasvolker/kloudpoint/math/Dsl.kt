package tomasvolker.kloudpoint.math

object V3 {

    val ZERO: DoubleVector3 = V3[0.0, 0.0, 0.0]

    operator fun get(value0: Double, value1: Double, value2: Double): DoubleVector3 =
            DoubleVector3(value0, value1, value2)

    operator fun get(value0: Number, value1: Number, value2: Number): DoubleVector3 =
        DoubleVector3(value0.toDouble(), value1.toDouble(), value2.toDouble())

}