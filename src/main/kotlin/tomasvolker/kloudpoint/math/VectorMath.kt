package tomasvolker.kloudpoint.math

import tomasvolker.numeriko.core.primitives.squared
import kotlin.math.sqrt

fun distanceBetween(
    vector1: DoubleVector3,
    vector2: DoubleVector3
): Double = sqrt(
    (vector2.v0 - vector1.v0).squared() +
    (vector2.v1 - vector1.v1).squared() +
    (vector2.v2 - vector1.v2).squared()
)

