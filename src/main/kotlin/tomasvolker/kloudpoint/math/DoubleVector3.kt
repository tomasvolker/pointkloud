package tomasvolker.kloudpoint.math

import tomasvolker.numeriko.core.interfaces.array1d.double.DoubleArray1D
import tomasvolker.numeriko.core.interfaces.array1d.double.defaultEquals
import tomasvolker.numeriko.core.interfaces.array1d.double.defaultHashCode
import tomasvolker.numeriko.core.interfaces.array1d.generic.Array1D
import tomasvolker.numeriko.core.interfaces.array1d.lowdim.generic.Vector3
import tomasvolker.numeriko.core.interfaces.array1d.generic.defaultEquals
import tomasvolker.numeriko.core.interfaces.arraynd.double.defaultToString
import kotlin.math.sqrt

class DoubleVector3(
    val v0: Double,
    val v1: Double,
    val v2: Double
): Vector3<Double>, DoubleArray1D {

    val value0: Double get() = v0
    val value1: Double get() = v1
    val value2: Double get() = v2

    override fun get(i0: Int): Double = getDouble(i0)

    override fun getDouble(i0: Int) = when(i0) {
        0 -> v0
        1 -> v1
        2 -> v2
        else -> throw IndexOutOfBoundsException("$i0")
    }

    override operator fun unaryPlus(): DoubleVector3 = this
    override operator fun unaryMinus(): DoubleVector3 = this

    operator fun plus (other: DoubleVector3): DoubleVector3 = elementWise(other) { t, o -> t + o }
    operator fun minus(other: DoubleVector3): DoubleVector3 = elementWise(other) { t, o -> t - o }
    operator fun times(other: DoubleVector3): DoubleVector3 = elementWise(other) { t, o -> t * o }
    operator fun div  (other: DoubleVector3): DoubleVector3 = elementWise(other) { t, o -> t / o }

    override operator fun plus (other: Double): DoubleVector3 = elementWise { it + other }
    override operator fun minus(other: Double): DoubleVector3 = elementWise { it - other }
    override operator fun times(other: Double): DoubleVector3 = elementWise { it * other }
    override operator fun div  (other: Double): DoubleVector3 = elementWise { it / other }

    override operator fun plus (other: Int): DoubleVector3 = elementWise { it + other }
    override operator fun minus(other: Int): DoubleVector3 = elementWise { it - other }
    override operator fun times(other: Int): DoubleVector3 = elementWise { it * other }
    override operator fun div  (other: Int): DoubleVector3 = elementWise { it / other }

    fun norm2(): Double = sqrt(norm2Squared())

    fun norm2Squared(): Double = v0 * v0 + v1 * v1 + v2 * v2

    fun normalized(): DoubleVector3 = this / norm2()

    infix fun inner(other: DoubleVector3): Double =
        this.v0 * other.v0 + this.v1 * other.v1 + this.v2 * other.v2

    infix fun vector(other: DoubleVector3): DoubleVector3 =
        DoubleVector3(
            this.v1 * other.v2 - this.v2 * other.v1,
            this.v2 * other.v0 - this.v0 * other.v2,
            this.v0 * other.v1 - this.v1 * other.v0
        )

    override fun equals(other: Any?): Boolean = when {
        other === this -> true
        other is DoubleVector3 -> this.v0 == other.v0 &&
                                  this.v1 == other.v1 &&
                                  this.v2 == other.v2
        other is DoubleArray1D -> this.defaultEquals(other)
        other is Array1D<*> -> this.defaultEquals(other)
        else -> false
    }

    override fun hashCode(): Int = this.defaultHashCode()

    override fun toString(): String = defaultToString(this)

}


inline fun DoubleVector3.elementWise(operation: (Double)->Double): DoubleVector3 =
    DoubleVector3(operation(v0), operation(v1), operation(v2))

inline fun DoubleVector3.elementWise(
    other: DoubleVector3,
    operation: (Double, Double)->Double
): DoubleVector3 =
    DoubleVector3(
        operation(this.v0, other.v0),
        operation(this.v1, other.v1),
        operation(this.v2, other.v2)
    )
