package tomasvolker.kloudpoint.math.doubleset

import java.util.*
import kotlin.math.min

class ContinuousDoubleSet(
    val containsMinusInfinity: Boolean,
    val borders: DoubleArray
) {

    init {
        Arrays.sort(borders)
    }

    val isDoubleSet: Boolean get() = containsMinusInfinity && borders.isEmpty()
    val isEmptySet: Boolean get() = !containsMinusInfinity && borders.isEmpty()

    private fun Int.isEven() = this % 2 == 0

    val containsPlusInfinity: Boolean get() = borders.size.isEven() == containsMinusInfinity

    operator fun contains(value: Double): Boolean =
        lowerBorderIndex(value).isEven() != containsMinusInfinity

    operator fun not(): ContinuousDoubleSet =
        ContinuousDoubleSet(
            containsMinusInfinity = !containsMinusInfinity,
            borders = borders
        )

    infix fun union(other: ContinuousDoubleSet): ContinuousDoubleSet =
        binaryOperation(this, other) { t, o -> t || o }

    infix fun intersect(other: ContinuousDoubleSet): ContinuousDoubleSet =
        binaryOperation(this, other) { t, o -> t && o }

    fun difference(other: ContinuousDoubleSet): ContinuousDoubleSet =
        binaryOperation(this, other) { t, o -> t && !o }

    private fun lowerBorderIndex(value: Double): Int =
        Arrays.binarySearch(borders, value)

    override fun equals(other: Any?): Boolean = when {
        other === this -> true
        other is ContinuousDoubleSet -> other.containsMinusInfinity == this.containsMinusInfinity &&
                                        other.borders.contentEquals(this.borders)
        else -> false
    }

    override fun hashCode(): Int = containsMinusInfinity.hashCode() + 31 * borders.hashCode()

    override fun toString(): String = buildString {

        if(isEmptySet) append("{}")

        if (containsMinusInfinity)
            append("(-inf;")

        var first = true

        for (i in borders.indices) {
            if (i.isEven() == containsMinusInfinity) {
                append("${borders[i]})")
            } else {
                if (!first) append(" U ")
                append("(${borders[i]};")
            }
            first = false
        }

        if (containsPlusInfinity)
            append("+inf)")

    }

}

inline fun binaryOperation(
    set1: ContinuousDoubleSet,
    set2: ContinuousDoubleSet,
    operation: (Boolean, Boolean)->Boolean
): ContinuousDoubleSet {

    val containsMinusInfinity = operation(
        set1.containsMinusInfinity,
        set2.containsMinusInfinity
    )

    val resultBorders = mutableListOf<Double>()

    var previous1 = set1.containsMinusInfinity
    var previous2 = set2.containsMinusInfinity

    var resultPrevious = containsMinusInfinity

    var index1 = 0
    var index2 = 0

    do {

        val border1 = set1.borders.getOrElse(index1) { Double.POSITIVE_INFINITY }
        val border2 = set2.borders.getOrElse(index2) { Double.POSITIVE_INFINITY }

        val border = min(border1, border2)
        val next1 = previous1 xor (border == border1)
        val next2 = previous2 xor (border == border2)

        val resultNext = operation(next1, next2)

        if (resultPrevious != resultNext)
            resultBorders.add(border)

        previous1 = next1
        previous2 = next2
        resultPrevious = resultNext

        if (border == border1) index1++
        if (border == border2) index2++

    } while(index1 < set1.borders.size || index2 < set2.borders.size)

    return ContinuousDoubleSet(
        containsMinusInfinity = containsMinusInfinity,
        borders = resultBorders.toDoubleArray()
    )
}

/*
data class EnumeratedDoubleSet(
    private val values: DoubleArray
): DoubleSet() {

    init {
        Arrays.sort(values)
    }

    override fun union(other: DoubleSet): DoubleSet = when(other) {
        is ContinuousDoubleSet ->
        is EnumeratedDoubleSet -> EnumeratedDoubleSet(this.values + other.values)
    }

    override fun contains(value: Double): Boolean =
            Arrays.binarySearch(values, value) >= 0

    override fun toString(): String =
        values.joinToString(prefix = "{", separator = ", ", postfix = "}")

    override fun equals(other: Any?): Boolean = when {
        other === this -> true
        other is EnumeratedDoubleSet -> this.values.contentEquals(other.values)
        else -> false
    }

    override fun hashCode(): Int = values.contentHashCode()

}

data class DoubleClosedRange(
    val min: Double,
    val max: Double
): DoubleSet() {

    init {
        require(min <= max)
    }

    override operator fun contains(value: Double): Boolean =
        min <= value && value <= max

    fun split(value: Double): Pair<DoubleSet, DoubleSet> =
            when {
                value < min -> Pair(EmptyDoubleSet, this)
                max < value -> Pair(this, EmptyDoubleSet)
                else -> Pair(
                    DoubleClosedRange(min, value),
                    DoubleClosedRange(value, max)
                )
            }

    override fun union(other: DoubleSet): DoubleSet = when(other) {
        is EmptyDoubleSet -> this
        is AllDoubleSet -> AllDoubleSet
        is EnumeratedDoubleSet -> DoubleSetUnion(
            other,
            EnumeratedDoubleSet(other.values.filter { it !in this }.toDoubleArray())
        )
        is DoubleClosedRange -> when {
            other.min < this.max -> DoubleClosedRange(this.min)
        }
        is DoubleSetUnion -> DoubleSetUnion(this union other.first, this union other.second)
    }

    override fun toString(): String = "[$min;$max]"
}


*/
