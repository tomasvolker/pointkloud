package tomasvolker.kloudpoint.math.doubleset

import tomasvolker.numeriko.core.primitives.squared

val allDoubles: ContinuousDoubleSet = ContinuousDoubleSet(containsMinusInfinity = true, borders = doubleArrayOf())
val emptyDoubleSet: ContinuousDoubleSet = ContinuousDoubleSet(containsMinusInfinity = false, borders = doubleArrayOf())
val positiveDoubles: ContinuousDoubleSet = ContinuousDoubleSet(containsMinusInfinity = false, borders = doubleArrayOf(0.0))
val negativeDoubles: ContinuousDoubleSet = ContinuousDoubleSet(containsMinusInfinity = true, borders = doubleArrayOf(0.0))

val ContinuousDoubleSet.centroid: Double get() = when {
    containsMinusInfinity && containsPlusInfinity -> -(!this).centroid
    containsMinusInfinity && !containsPlusInfinity -> Double.NEGATIVE_INFINITY
    !containsMinusInfinity && containsPlusInfinity -> Double.POSITIVE_INFINITY
    else -> {
        var result = 0.0

        for (i in 0 until borders.size step 2) {
            result -= borders[i].squared() / 2
        }

        for (i in 1 until borders.size step 2) {
            result += borders[i].squared() / 2
        }

        result / length
    }
}

fun doublesGreaterThan(value: Double): ContinuousDoubleSet =
        ContinuousDoubleSet(containsMinusInfinity = false, borders = doubleArrayOf(value))

fun doublesLessThan(value: Double): ContinuousDoubleSet =
    ContinuousDoubleSet(containsMinusInfinity = true, borders = doubleArrayOf(value))

fun ContinuousDoubleSet.split(value: Double): Pair<ContinuousDoubleSet, ContinuousDoubleSet> =
        Pair(
            doublesLessThan(value) intersect this,
            doublesGreaterThan(value) intersect this
        )

val ContinuousDoubleSet.length: Double get() {
    if (containsMinusInfinity || containsPlusInfinity)
        return Double.POSITIVE_INFINITY

    var result = 0.0

    for (i in 0 until borders.size step 2) {
        result -= borders[i]
    }

    for (i in 1 until borders.size step 2) {
        result += borders[i]
    }

    return result
}

val ContinuousDoubleSet.isCompact: Boolean get() = !containsMinusInfinity && !containsPlusInfinity
val ContinuousDoubleSet.isConvex: Boolean get() = borders.size <= 2
val ContinuousDoubleSet.isInterval: Boolean get() = isCompact && isConvex
val ContinuousDoubleSet.max: Double get() = if(containsPlusInfinity) Double.POSITIVE_INFINITY else borders.last()
val ContinuousDoubleSet.min: Double get() = if(containsMinusInfinity) Double.NEGATIVE_INFINITY else borders.first()
