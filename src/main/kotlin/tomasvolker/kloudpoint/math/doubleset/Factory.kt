package tomasvolker.kloudpoint.math.doubleset

operator fun Double.rangeTo(other: Double): ContinuousDoubleSet =
    closedRange(this, other)

fun closedRange(from: Double, until: Double): ContinuousDoubleSet = when {
    from <= until -> ContinuousDoubleSet(false, doubleArrayOf(from, until))
    else -> emptyDoubleSet
}


inline fun computeRange(size: Int, accessor: (Int)->Double): ContinuousDoubleSet {
    var min = Double.POSITIVE_INFINITY
    var max = Double.NEGATIVE_INFINITY
    for (i in 0 until size) {
        val value = accessor(i)

        if (value < min)
            min = value

        if(value > max)
            max = value

    }
    return closedRange(from = min, until = max)
}
