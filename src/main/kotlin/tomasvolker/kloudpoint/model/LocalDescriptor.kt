package tomasvolker.kloudpoint.model

import tomasvolker.kloudpoint.model.primitives.Point

data class LocalDescriptor<out F>(
    val point: Point,
    val feature: F
)
