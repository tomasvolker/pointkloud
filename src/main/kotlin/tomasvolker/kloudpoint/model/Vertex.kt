package tomasvolker.kloudpoint.model

data class Vertex(
    val x: Float,
    val y: Float,
    val z: Float,
    val reflectance: Float,
    val label: Int,
    val clazz: Int
)
