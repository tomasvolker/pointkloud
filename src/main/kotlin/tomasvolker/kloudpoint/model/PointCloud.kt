package tomasvolker.kloudpoint.model

import tomasvolker.kloudpoint.math.DoubleVector3
import tomasvolker.kloudpoint.math.V3
import tomasvolker.kloudpoint.math.doubleset.computeRange
import tomasvolker.kloudpoint.model.primitives.Point

val DoubleVector3.x get(): Double = value0
val DoubleVector3.y get(): Double = value1
val DoubleVector3.z get(): Double = value2

typealias PointCloud = List<Point>

fun List<Point>.computeCentroid(): Point {
    var x = 0.0
    var y = 0.0
    var z = 0.0

    for (point in this) {
        x += point.x
        y += point.y
        z += point.z
    }

    val size = size
    return V3[x / size, y / size, z / size]
}

fun List<Point>.computeBounds(): BoundingBox3D =
    BoundingBox3D(
        listOf(
            computeRange(size) { i -> this[i].x },
            computeRange(size) { i -> this[i].y },
            computeRange(size) { i -> this[i].z }
        )
    )
