package tomasvolker.kloudpoint.model

enum class ObjectCategory(val value: Int) {
    BACKGROUND(0),
    FACADE(1),
    GROUND(2),
    CARS(4),
    LIGHT_POLE(7),
    PEDESTRIANS(9),
    MOTORCYCLES(10),
    TRAFFIC_SIGNS(14),
    TRASH_CANS(15),
    LIGHT_WALL(19),
    BALCONY_PLANT(20),
    PARKING_METER(21),
    FAST_PEDESTRIAN(22),
    WALL_SIGN(23),
    PEDESTRIAN_PLUS_SOMETHING(24),
    NOISE(25),
    POT_PLANT(26);

    companion object {

        fun fromValue(value: Int): ObjectCategory =
                ObjectCategory.values().firstOrNull { it.value == value } ?: throw NoSuchElementException("$value")

    }

}

object VertexClass {

    val background     = 0
    val facade         = 1
    val ground         = 2
    val cars           = 4
    val lightPole      = 7
    val pedestrians    = 9
    val motorcycles    = 10
    val trafficSigns   = 14
    val trashCans      = 15
    val lightWall      = 19
    val balconyPlant   = 20
    val parkingMeter   = 21
    val fastPedestrian = 22
    val wallSign       = 23
    val pedestrianPlusSomething = 24
    val noise          = 25
    val potPlant       = 26

}

val clazzMap = mapOf(
    0 to "Background",
    1 to "Facade",
    2 to "Ground",
    4 to "Cars",
    7 to "Light pole",
    9 to "Pedestrians",
    10 to "Motorcycles",
    14 to "Traffic signs",
    15 to "Trash can",
    19 to "Light Wall",
    20 to "Balcony Plant",
    21 to "Parking meter",
    22 to "Fast pedestrian",
    23 to "Wall sign",
    24 to "Pedestrian + something",
    25 to "Noise",
    26 to "Pot plant"
)
