package tomasvolker.kloudpoint.model

import boofcv.struct.image.GrayF32
import boofcv.struct.image.GrayF64
import boofcv.struct.image.ImageBase

data class IntPoint2(
    val x: Int,
    val y: Int
) {

    operator fun plus(other: IntPoint2): IntPoint2 =
        IntPoint2(this.x + other.x, this.y + other.y)

    operator fun minus(other: IntPoint2): IntPoint2 =
        IntPoint2(this.x - other.x, this.y - other.y)

    override fun toString(): String = "($x, $y)"

}

object I2 {
    operator fun get(x: Int, y: Int) = IntPoint2(x, y)
}

fun IntPoint2.fourNeighborhood() =
    listOf(
        I2[x, y-1],
        I2[x-1, y],
        I2[x, y+1],
        I2[x+1, y]
    )

fun IntPoint2.eightNeighborhood() =
    listOf(
        I2[x-1, y-1],
        I2[x  , y-1],
        I2[x+1, y-1],
        I2[x-1, y  ],
        I2[x+1, y  ],
        I2[x-1, y+1],
        I2[x  , y+1],
        I2[x+1, y+1]
    )

fun ImageBase<*>.isInBounds(point: IntPoint2): Boolean =
    isInBounds(point.x, point.y)

operator fun GrayF64.get(point: IntPoint2): Double = get(point.x, point.y)
operator fun GrayF64.set(point: IntPoint2, value: Double) { set(point.x, point.y, value) }

operator fun GrayF32.get(point: IntPoint2): Float = get(point.x, point.y)
operator fun GrayF32.set(point: IntPoint2, value: Float) { set(point.x, point.y, value) }
