package tomasvolker.kloudpoint.model.primitives

import tomasvolker.kloudpoint.math.DoubleVector3

data class Triangle(
    val v0: Point,
    val v1: Point,
    val v2: Point
) {

    val normal: DoubleVector3 get() = (v1 - v0) vector (v2 - v0)

}