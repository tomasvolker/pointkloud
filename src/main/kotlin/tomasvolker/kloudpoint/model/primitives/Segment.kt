package tomasvolker.kloudpoint.model.primitives

import tomasvolker.kloudpoint.math.DoubleVector3
import tomasvolker.kloudpoint.math.distanceBetween

data class Segment(
    val start: Point,
    val end: Point
) {

    val length: Double get() = distanceBetween(start, end)

    val delta: DoubleVector3 get() = end - start

}

