package tomasvolker.kloudpoint.model.primitives

import tomasvolker.kloudpoint.math.DoubleVector3

typealias Vector3 = DoubleVector3
typealias Point = Vector3
