package tomasvolker.kloudpoint.model

import tomasvolker.kloudpoint.math.DoubleVector3
import tomasvolker.kloudpoint.math.V3
import tomasvolker.kloudpoint.math.doubleset.*

interface BoundingBoxND {

    val rangeList: List<ContinuousDoubleSet>

    fun range(index: Int): ContinuousDoubleSet = rangeList[index]

    val dimension: Int get() = rangeList.size

    val isEmpty: Boolean get() = rangeList.any { it.isEmptySet }

}

data class BoundingBox3D(
    override val rangeList: List<ContinuousDoubleSet>
): BoundingBoxND {

    val xRange: ContinuousDoubleSet get() = rangeList[0]
    val yRange: ContinuousDoubleSet get() = rangeList[1]
    val zRange: ContinuousDoubleSet get() = rangeList[2]

    val center: DoubleVector3
        get() = V3[xRange.centroid, yRange.centroid, zRange.centroid]

    override fun toString(): String = "BoundingBox(xRange = $xRange, yRange = $yRange, zRange = $zRange)"

    operator fun contains(point: DoubleVector3): Boolean =
            point.x in xRange && point.y in yRange && point.z in zRange

}

fun BoundingBox3D.split(dimension: Int, value: Double): Pair<BoundingBox3D, BoundingBox3D> =
        Pair(
            BoundingBox3D(
                rangeList.mapIndexed { i, range ->
                    if (i == dimension)
                        range intersect doublesLessThan(value)
                    else
                        range
                }
            ),
            BoundingBox3D(
                rangeList.mapIndexed { i, range ->
                    if (i == dimension)
                        range intersect doublesGreaterThan(value)
                    else
                        range
                }
            )
        )

