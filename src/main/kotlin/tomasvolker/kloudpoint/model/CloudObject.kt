package tomasvolker.kloudpoint.model

class CloudObject(
    val identifier: Int,
    val category: ObjectCategory,
    val pointCloud: PointCloud
) {

    val id: Int get() = identifier

}

fun CloudObject.printStats() {

    val boundingBox = pointCloud.computeBounds()

    println("""
        Cloud Object $identifier
        Type: $category
        Point count: ${pointCloud.size}
        Centroid: ${pointCloud.computeCentroid()}
        X range: ${boundingBox.xRange}
        Y range: ${boundingBox.yRange}
        Z range: ${boundingBox.zRange}
    """.trimIndent())

}