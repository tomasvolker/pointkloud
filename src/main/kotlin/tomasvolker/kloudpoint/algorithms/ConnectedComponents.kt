package tomasvolker.kloudpoint.algorithms

import java.util.*



fun <T> Iterable<T>.connectedComponents(
    depthFirst: Boolean = false,
    neighbors: (T)->Iterable<T>
): List<Set<T>> {

    val remaining = toMutableSet()

    val result = mutableListOf<Set<T>>()

    while(remaining.isNotEmpty()) {

        val component = connectedComponent(
            node = remaining.first(),
            neighbors = neighbors,
            depthFirst = depthFirst
        )

        result.add(component)
        remaining.removeAll(component)

    }

    return result
}

fun <T> connectedComponent(
    node: T,
    neighbors: (T)->Iterable<T>,
    depthFirst: Boolean = false
): Set<T> {

    val visited = mutableSetOf<T>()
    visited.add(node)

    val queue = ArrayDeque<T>()
    queue.add(node)

    while(queue.isNotEmpty()) {

        val current = queue.pop()

        val newNodes = neighbors(current).filter { (it !in visited) }

        for(neighbor in newNodes) {

            if (neighbor !in visited) {
                visited.add(neighbor)

                if (depthFirst)
                    queue.addFirst(neighbor)
                else
                    queue.add(neighbor)

            }

        }

    }

    return visited
}
