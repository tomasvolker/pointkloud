package tomasvolker.kloudpoint.algorithms.hernandezmarcotegui

import boofcv.struct.image.GrayF64
import boofcv.struct.image.ImageBase
import org.openrndr.color.ColorRGBa
import org.openrndr.math.Vector2
import tomasvolker.kloudpoint.algorithms.connectedComponents
import tomasvolker.kloudpoint.div
import tomasvolker.kloudpoint.image.*
import tomasvolker.kloudpoint.io.loadRueMadame
import tomasvolker.kloudpoint.math.doubleset.*
import tomasvolker.kloudpoint.measured
import tomasvolker.kloudpoint.model.*
import tomasvolker.kloudpoint.model.primitives.Point
import tomasvolker.kloudpoint.toPath
import tomasvolker.kloudpoint.visualization.*
import tomasvolker.kloudpoint.visualization.model.color
import tomasvolker.koofcv.constructor.grayF64
import tomasvolker.koofcv.statistics.max
import tomasvolker.koofcv.statistics.min
import tomasvolker.numeriko.core.interfaces.factory.array2D
import tomasvolker.openrndr.math.primitives.d
import java.awt.image.BufferedImage
import kotlin.math.ceil
import kotlin.math.floor
import kotlin.random.Random

class HernandezMarcoteguiSegmenter(
    val pixelSize: Double = 0.2,
    val flatZoneDelta: Double = 2.0,
    val artifactHeightThreshold: Double = 0.1,
    val localMaxHeight: Double = 0.3
) {

    var image = GrayF64(0, 0)
    var projection = TopProjection(image, Vector2.ZERO, 1.0)
    var flatZones: List<Set<IntPoint2>> = emptyList()
    var groundMask = emptySet<IntPoint2>()
    var border = emptySet<IntPoint2>()
    var maskedImage = GrayF64(0, 0)
    var filled = GrayF64(0, 0)
    var inverted = GrayF64(0, 0)
    var invertedWithBorder = GrayF64(0, 0)
    var invertedFilled = GrayF64(0, 0)
    var bumps = GrayF64(0, 0)
    var hMax = GrayF64(0, 0)
    var artifactSeeds: List<Set<IntPoint2>> = emptyList()
    var artifactSeedsFiltered: List<Set<IntPoint2>> = emptyList()
    var artifacts = emptyMap<IntPoint2, Int>()
    var result = Result(emptyList(), emptyList(), emptyList())

    data class Result(
        val facade: PointCloud,
        val ground: PointCloud,
        val artifacts: List<PointCloud>
    )

    fun segment(pointCloud: PointCloud): Result {

        projection = pointCloud.buildTopProjectionImage()

        image = projection.image

        //showImage3D(image, pixelSize = pixelSize)

        groundMask = image.segmentGround()

        artifacts = image.detectArtifacts(groundMask)

        result = pointCloud.projectPoints(projection, groundMask, artifacts)

        return result
    }

    fun PointCloud.projectPoints(
        projection: TopProjection,
        groundMask: Set<IntPoint2>,
        artifacts: Map<IntPoint2, Int>
    ): Result {

        val ground = mutableListOf<Point>()
        val facade = mutableListOf<Point>()
        val segmentedArtifacts = mutableMapOf<Int, MutableList<Point>>()

        forEach { point ->
            val pixel = projection.pixelFor(point)

            if (pixel in groundMask) {
                val label = artifacts[pixel]
                if (label != null) {
                    segmentedArtifacts.getOrPut(label) { mutableListOf() }.add(point)
                } else {
                    ground.add(point)
                }
            } else {
                facade.add(point)
            }
        }

        return Result(
            facade = facade,
            ground = ground,
            artifacts = segmentedArtifacts.values.toList()
        )
    }

    data class TopProjection(
        val image: GrayF64,
        val delta: Vector2,
        val pixelSize: Double
    ) {

        fun pixelFor(point: Point): IntPoint2 =
            I2[floor((point.x - delta.x) / pixelSize).toInt(),
               floor((point.y - delta.y) / pixelSize).toInt()]

    }

    fun PointCloud.buildTopProjectionImage(): TopProjection {

        val margin = 1.0

        val bounds = computeBounds()
        val xRange = closedRange(bounds.xRange.min - margin, bounds.xRange.max + margin)
        val yRange = closedRange(bounds.yRange.min - margin, bounds.yRange.max + margin)

        val width = ceil(xRange.length / pixelSize).toInt()
        val height = ceil(yRange.length / pixelSize).toInt()

        val minZ = this.minBy { it.z }?.z ?: 0.0

        val result = grayF64(width, height) { _, _ -> minZ }

        val minX = xRange.min
        val minY = yRange.min

        forEach { point ->
            val x = floor((point.x - minX) / pixelSize).toInt()
            val y = floor((point.y - minY) / pixelSize).toInt()
            if(result[x, y] < point.z) {
                result[x, y] = point.z
            }
        }

        return TopProjection(
            image = result,
            delta = Vector2(minX, minY),
            pixelSize = pixelSize
        )
    }

    fun PointCloud.buildAccumulatorImage(): GrayF64 =
        this.topAccumulation(0.2, margin = 1.0)

    fun GrayF64.segmentGround(): Set<IntPoint2> =
        flatZones(flatZoneDelta)
            .also { flatZones = it }
            .filterNot { zone -> zone.any { this.isInBorder(it) } }
            .maxBy { it.size } ?: error("No connected component")

    fun GrayF64.detectArtifacts(groundMask: Set<IntPoint2>): Map<IntPoint2, Int> {

        maskedImage = this.mask { x, y -> I2[x, y] in groundMask }

        filled = maskedImage.fillHoles()

        border = groundMask.outerBorder()

        val min = filled.min()
        val max = filled.max()

        inverted = filled.elementWise { if(it == min) -max else -it }

        invertedWithBorder = inverted.clone()

        border.forEach { invertedWithBorder[it] = inverted.maxAround(it, 20) }

        invertedFilled = invertedWithBorder.fillHoles()
        bumps = (invertedFilled - invertedWithBorder)
            .elementWise { if (it < artifactHeightThreshold) 0.0 else it }
            .open8()

        val artifactMask = bumps.points().filter { bumps[it] > 0.0 }.toSet()

        hMax = reconstructionByDilatation(
            marker = bumps,
            mask = bumps - localMaxHeight
        )

        val maxSet = hMax.points()
            .filter { hMax[it] > artifactHeightThreshold && hMax.isLocalMaxima(it) }
            .toSet()

        artifactSeeds = maxSet.connectedComponents { point ->
            point.eightNeighborhood().filter { it in maxSet }
        }
            //.filter { component -> component.outerBorder().all { hMax[component.first()] >= hMax[it] } }
        artifactSeedsFiltered = artifactSeeds.filter { it.size > 25 }

        val segmented = watershed(
            image = bumps.toGrayF32(),
            seeds = artifactSeedsFiltered,
            mask = artifactMask
        )

        //showSegmentedPoints(segmented, image.width, image.height)

        return segmented
    }

    fun GrayF64.pointsAround(point: IntPoint2, radius: Int): Sequence<IntPoint2> =
        (point.x-radius..point.x+radius).asSequence().flatMap { x ->
            (point.y-radius..point.y+radius).asSequence().map { y ->
                I2[x, y]
            }
        }.filter { isInBounds(it) }

    fun GrayF64.maxAround(point: IntPoint2, radius: Int = 10): Double =
        pointsAround(point, radius).map { this[it] }.max() ?: error("ignore")

}

fun main() {

    val directory = "data".toPath()
    val file = directory/"GT_Madame1_2.ply"

    println("Loading dataset")

    val (rueMadame, millis) = measured { loadRueMadame(file.toFile()) }

    println("Data loaded in ${millis / 1000.0} seconds")

    val pointCloud = rueMadame.flatMap { it.pointCloud }

    val segmenter = HernandezMarcoteguiSegmenter(
        pixelSize = 0.1,
        flatZoneDelta = 1.0,
        artifactHeightThreshold = 0.1,
        localMaxHeight = 0.2
    )

/*
    val segmenter = HernandezMarcoteguiSegmenter(
        flatZoneDelta = 2.0,
        artifactHeightThreshold = 0.1,
        localMaxHeight = 0.7
    )
*/

    val (facade, ground, artifacts) = segmenter.segment(pointCloud)

    showPlot3D {

        initialPosition = pointCloud.computeCentroid()

        scatter {
            pointList = facade
            color = ColorRGBa.RED
        }

        scatter {
            pointList = ground
            color = ColorRGBa.GRAY
        }

        artifacts.forEach { points ->

            scatter {
                pointList = points
                //color = ColorRGBa.GREEN.shade(0.8)
                color = Random.nextColorRGB()
            }

        }

    }

}

fun <K, V> Map<K, V>.inverseMap(): Map<V, Set<K>> {
    val result = mutableMapOf<V, MutableSet<K>>()

    forEach { key, value ->
        result.getOrPut(value) { mutableSetOf() }.add(key)
    }

    return result
}

fun ImageBase<*>.points(): Sequence<IntPoint2> =
    (0 until width).asSequence().flatMap { x ->
        (0 until height).asSequence().map { y ->
            I2[x, y]
        }
    }

fun showImage3D(image: GrayF64, pixelSize: Double = 1.0) {

    val max = image.max()
    val min = image.min()

    val mapper = heatColorMapper(min, max)

    showPlot3D {

        initialPosition = Point(pixelSize * image.width / 2.0, pixelSize * image.height / 2.0, 0.0)

        surface {

            colorMapper = { mapper(it.z) }

            pointGrid = array2D(image.width, image.height) { x, y ->
                Point(pixelSize * x.d, pixelSize * y.d, image[x, y])
            }

        }

    }

}

fun showSegmentedPoints(map: Map<IntPoint2, Int>, width: Int, height: Int) {

    val visible = BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)
    val colorMap = mutableMapOf<Int, ColorRGBa>()
    map.forEach { point, i ->
        visible[point.x, point.y] = colorMap.getOrPut(i) { Random.nextColorRGB() }
    }

    showImage(visible)
}

fun Set<IntPoint2>.buildBinaryImage(width: Int, height: Int): BufferedImage {

    val result = BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)

    forEach {
        result[it] = ColorRGBa.WHITE
    }

    return result
}