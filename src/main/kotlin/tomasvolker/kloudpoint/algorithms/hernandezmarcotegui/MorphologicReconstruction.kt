package tomasvolker.kloudpoint.algorithms.hernandezmarcotegui

import boofcv.struct.image.GrayF64
import tomasvolker.kloudpoint.model.*
import tomasvolker.koofcv.interation.forEachIndexXY
import tomasvolker.koofcv.interation.forEachPixel
import java.util.*
import kotlin.math.min

fun GrayF64.isLocalMinima(point: IntPoint2) =
        point.fourNeighborhood()
            .filter { isInBounds(it) }
            .all { this[it] >= this[point] }

fun GrayF64.isLocalMaxima(point: IntPoint2) =
    point.fourNeighborhood()
        .filter { isInBounds(it) }
        .all { this[it] <= this[point] }

fun reconstructionByDilatation(
    marker: GrayF64,
    mask: GrayF64,
    destination: GrayF64? = null
): GrayF64 {

    val result = destination?.apply { setTo(mask) } ?: mask.clone()

    val queue = ArrayDeque<IntPoint2>()
    marker.forEachPixel { x, y, value ->
        if (marker.isLocalMaxima(I2[x, y]))
            queue.add(I2[x, y])
    }

    while(queue.isNotEmpty()) {
        val current = queue.pop()

        for(neighbor in current.eightNeighborhood()) {

            if(marker.isInBounds(neighbor)) {

                val candidate = min(result[current], marker[neighbor])

                if (candidate > result[neighbor]) {
                    result[neighbor] = candidate
                    queue.addLast(neighbor)
                }

            }

        }

    }

    return result
}


fun GrayF64.open8(destination: GrayF64? = null): GrayF64 {

    val result = destination ?: createSameShape()

    result.forEachIndexXY { index, x, y ->
        result.data[index] = I2[x, y]
            .let { it.eightNeighborhood() + it }
            .filter { isInBounds(it) }
            .map { this[it] }.max() ?: error("")
    }

    return result
}