package tomasvolker.kloudpoint.algorithms.hernandezmarcotegui

import boofcv.struct.image.GrayF64
import tomasvolker.kloudpoint.algorithms.connectedComponents
import tomasvolker.kloudpoint.model.*
import tomasvolker.kloudpoint.visualization.nextColorRGB
import tomasvolker.kloudpoint.visualization.set
import java.awt.image.BufferedImage
import kotlin.math.abs
import kotlin.random.Random

fun GrayF64.flatZones(delta: Double): List<Set<IntPoint2>> {

    val points = (0 until width).flatMap { x ->
        (0 until height).map { y -> IntPoint2(x, y) }
    }

    return points.connectedComponents { point ->
        point.eightNeighborhood().filter { neighbor ->
            isInBounds(neighbor) && abs(this[point] - this[neighbor]) < delta
        }
    }
}

fun List<Set<IntPoint2>>.mergeImage(width: Int, height: Int): BufferedImage {

    val result = BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)

    forEach { cluster ->

        val color = Random.nextColorRGB()

        cluster.forEach {
            result[it] = color
        }
    }

    return result
}
