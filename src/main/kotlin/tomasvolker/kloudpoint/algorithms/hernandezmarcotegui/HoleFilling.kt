package tomasvolker.kloudpoint.algorithms.hernandezmarcotegui

import boofcv.struct.image.GrayF64
import tomasvolker.kloudpoint.image.isInBorder
import tomasvolker.kloudpoint.model.*
import tomasvolker.koofcv.constructor.grayF64
import tomasvolker.koofcv.interation.forEachPixel
import tomasvolker.koofcv.statistics.max
import java.util.*
import kotlin.math.max
import kotlin.math.min

fun GrayF64.fillHoles(): GrayF64 {

    val max = max()

    val result = grayF64(width, height) { x, y ->
        if (isInBorder(x, y))
            this[x, y]
        else
            max
    }

    val queue = ArrayDeque<IntPoint2>()

    for (x in 0 until width) {
        queue.add(I2[x, 0])
        queue.add(I2[x, height-1])
    }

    for (y in 0 until height) {
        queue.add(I2[0, y])
        queue.add(I2[width-1, y])
    }

    while(queue.isNotEmpty()) {
        val current = queue.pop()

        for(neighbor in current.eightNeighborhood()) {

            if(isInBounds(neighbor)) {

                val candidate = max(result[current], this[neighbor])

                if (candidate < result[neighbor]) {
                    result[neighbor] = candidate
                    queue.addLast(neighbor)
                }

            }

        }

    }

    return result
}

