package tomasvolker.kloudpoint.algorithms.hernandezmarcotegui

import boofcv.struct.image.GrayF64
import tomasvolker.kloudpoint.model.IntPoint2
import tomasvolker.kloudpoint.model.eightNeighborhood
import tomasvolker.kloudpoint.math.doubleset.closedRange
import tomasvolker.kloudpoint.math.doubleset.length
import tomasvolker.kloudpoint.math.doubleset.max
import tomasvolker.kloudpoint.math.doubleset.min
import tomasvolker.kloudpoint.model.*
import tomasvolker.koofcv.constructor.grayF64
import kotlin.math.ceil
import kotlin.math.floor


fun PointCloud.topProjection(
    pixelSize: Double = 1.0,
    margin: Double = 0.0
): GrayF64 {

    val bounds = computeBounds()
    val xRange = closedRange(bounds.xRange.min - margin, bounds.xRange.max + margin)
    val yRange = closedRange(bounds.yRange.min - margin, bounds.yRange.max + margin)

    val width = ceil(xRange.length / pixelSize).toInt()
    val height = ceil(yRange.length / pixelSize).toInt()

    val minZ = this.minBy { it.z }?.z ?: 0.0

    val result = grayF64(width, height) { _, _ -> minZ }

    val minX = xRange.min
    val minY = yRange.min

    forEach { point ->
        val x = floor((point.x - minX) / pixelSize).toInt()
        val y = floor((point.y - minY) / pixelSize).toInt()
        if(result[x, y] < point.z) {
            result[x, y] = point.z
        }
    }

    return result
}

fun PointCloud.topAccumulation(pixelSize: Double = 1.0, margin: Double = 0.0): GrayF64 {

    val bounds = computeBounds()

    val xRange = closedRange(bounds.xRange.min - margin, bounds.xRange.max + margin)
    val yRange = closedRange(bounds.yRange.min - margin, bounds.yRange.max + margin)

    val width = ceil(xRange.length / pixelSize).toInt()
    val height = ceil(yRange.length / pixelSize).toInt()

    val result = GrayF64(width, height)

    val minX = xRange.min
    val minY = yRange.min

    forEach { point ->
        val x = floor((point.x - minX) / pixelSize).toInt()
        val y = floor((point.y - minY) / pixelSize).toInt()
        result[x, y]++
    }

    return result
}

fun Set<IntPoint2>.outerBorder(): Set<IntPoint2> {

    val result = mutableSetOf<IntPoint2>()

    forEach { node ->
        node.eightNeighborhood().forEach { neighbor ->
            if(neighbor !in this) result.add(neighbor)
        }
    }

    return result
}


