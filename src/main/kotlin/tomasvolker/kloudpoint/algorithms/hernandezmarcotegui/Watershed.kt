package tomasvolker.kloudpoint.algorithms.hernandezmarcotegui

import boofcv.struct.image.GrayF32
import org.openrndr.color.ColorRGBa
import tomasvolker.kloudpoint.boofcv.gradient
import tomasvolker.kloudpoint.image.elementWise
import tomasvolker.kloudpoint.model.*
import tomasvolker.kloudpoint.visualization.set
import tomasvolker.kloudpoint.visualization.showImage
import tomasvolker.koofcv.constructor.grayF32
import tomasvolker.numeriko.core.primitives.indicator
import tomasvolker.openrndr.math.primitives.f
import java.awt.image.BufferedImage
import java.util.*
import kotlin.math.hypot

fun watershed(
    image: GrayF32,
    seeds: List<Set<IntPoint2>>,
    mask: Set<IntPoint2> = emptySet()
): Map<IntPoint2, Int> {

    data class Frontier(
        val point: IntPoint2,
        val priority: Float
    ): Comparable<Frontier> {
        override fun compareTo(other: Frontier): Int =
                this.priority.compareTo(other.priority)
    }

    val gradient: GrayF32 = image.gradient().let { (dx, dy) ->
        dx.elementWise(dy) { x, y -> hypot(x, y) }
    }

    val labelMap = mutableMapOf<IntPoint2, Int>()
    seeds.forEachIndexed { i, points ->
        points.forEach { labelMap[it] = i }
    }

    val queue = PriorityQueue<Frontier>()

    labelMap.keys.forEach { point ->
        point.eightNeighborhood()
            .filter { image.isInBounds(it) && it !in labelMap && it in mask }
            .forEach {
                queue.add(Frontier(it, gradient[it]))
            }
    }

    while(queue.isNotEmpty()) {
        val (point, _) = queue.poll()

        if (point in labelMap) continue

        val (labeled, unlabeled) = point
            .eightNeighborhood()
            .filter { image.isInBounds(it) && it in mask }
            .partition { it in labelMap }

        if (labeled.isNotEmpty()) {
            val first = labelMap.getValue(labeled.first())
            if (labeled.all { labelMap[it] == first }) {
                labelMap[point] = first
                unlabeled.forEach {
                    queue.add(Frontier(it, gradient[it]))
                }
            }
        }

    }

    return labelMap
}

fun main() {

    val image = grayF32(100, 100) { x, y ->
        (hypot(x - 50.0, y - 50.0) < 20.0 || x in 40..60).indicator().f
    }

    val segmentation = watershed(
        image,
        listOf(
            setOf(
                I2[50, 50]
            ),
            setOf(
                I2[0, 50]
            ),
            setOf(
                I2[99, 50]
            )
        ),
        image.points().filter { it.y in 20..80 }.toSet()
    )

    val visible = BufferedImage(image.width, image.height, BufferedImage.TYPE_INT_ARGB)

    segmentation.forEach { point, i ->
        val color = when(i) {
            0 -> ColorRGBa.RED
            1 -> ColorRGBa.BLUE
            2 -> ColorRGBa.GREEN
            else -> ColorRGBa.TRANSPARENT
        }
        visible[point.x, point.y] = color
    }

    showImage(visible)

}