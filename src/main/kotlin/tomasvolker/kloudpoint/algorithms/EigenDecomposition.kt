package tomasvolker.kloudpoint.algorithms

import org.ejml.dense.row.factory.DecompositionFactory_DDRM
import tomasvolker.numeriko.core.functions.normalized
import tomasvolker.numeriko.core.functions.times
import tomasvolker.numeriko.core.interfaces.array1d.double.DoubleArray1D
import tomasvolker.numeriko.core.interfaces.array2d.double.DoubleArray2D
import tomasvolker.numeriko.core.interfaces.array2d.generic.indices0
import tomasvolker.numeriko.core.interfaces.factory.doubleArray1D
import tomasvolker.numeriko.core.interfaces.factory.doubleArray2D
import tomasvolker.numeriko.core.interfaces.factory.doubleDiagonal


data class EigenVector(
        val value: Double,
        val vector: DoubleArray1D
)

class EigenDecomposition(
        val eigenVectorList: List<EigenVector>
) {

    val size get() = eigenVectorList.size

    fun diagonal(): DoubleArray2D = doubleDiagonal(size) { i -> eigenVectorList[i].value }

    val eigenValues: DoubleArray1D get() =
        doubleArray1D(size) { i -> eigenVectorList[i].value }

    fun vectorMatrix(): DoubleArray2D = doubleArray2D(size, size) { i0, i1 ->
        eigenVectorList[i1].vector[i0]
    }

    operator fun component1(): DoubleArray2D = vectorMatrix()
    operator fun component2(): DoubleArray2D = diagonal()

    override fun toString(): String =
            "EigenDecomposition(eigenVectorList = $eigenVectorList)"

}


fun DoubleArray2D.eigenDecomposition(symetric: Boolean = false): EigenDecomposition {
    val decomposition = DecompositionFactory_DDRM.eig(shape0, true, symetric)

    if (!decomposition.decompose(toEjmlMatrix()))
        throw RuntimeException("Could not decompose")

    return EigenDecomposition(
            indices0.map { i ->
                EigenVector(
                        value = decomposition.getEigenvalue(i).real,
                        vector = decomposition.getEigenVector(i).toDoubleArray1D()
                )
            }
    )
}

fun List<DoubleArray1D>.orthogonalize(): List<DoubleArray1D> {

    val result = mutableListOf(this.first().normalized())

    this.drop(1).forEach { vector ->
        result.add(
            (vector - sumVector(result.indices) { i -> (vector colinearityFactor result[i]) * result[i] }).normalized()
        )

    }

    return result
}
