package tomasvolker.kloudpoint.algorithms.kdtree

import tomasvolker.kloudpoint.math.DoubleVector3
import tomasvolker.kloudpoint.math.doubleset.centroid
import tomasvolker.kloudpoint.math.doubleset.length
import tomasvolker.kloudpoint.model.*
import tomasvolker.numeriko.core.interfaces.array1d.double.DoubleArray1D

sealed class DoubleKDTree<V: DoubleArray1D>

class DoubleKDNode<V: DoubleArray1D>(
    val dimension: Int = 0,
    val partition: Double = 0.0,
    val less: DoubleKDTree<V> = DoubleKDList(),
    val greater: DoubleKDTree<V> = DoubleKDList()
): DoubleKDTree<V>()

class DoubleKDList<V: DoubleArray1D>(
    val values: List<V> = emptyList()
): DoubleKDTree<V>()


data class SpacePartition(
    val dimension: Int,
    val value: Double
)

typealias PartitionPolicy = (points: List<DoubleVector3>, bounds: BoundingBox3D)-> SpacePartition?

class MidRangePolicy(val maxSize: Int): PartitionPolicy {

    override fun invoke(points: List<DoubleVector3>, bounds: BoundingBox3D): SpacePartition? {

        if (points.size <= maxSize) return null

        val (dimension, maxRange) = bounds.rangeList
            .asSequence()
            .mapIndexed { i, range -> i to range }
            .maxBy { it.second.length } ?: error("Rangelist cannot be empty")

        return SpacePartition(dimension, maxRange.centroid)
    }

}

fun List<DoubleVector3>.buildKdTree(
    bounds: BoundingBox3D = computeBounds(),
    partitionPolicy: PartitionPolicy = MidRangePolicy(
        maxSize = 100
    )
): DoubleKDTree<DoubleVector3> {

    return when {

        size <= 1 -> DoubleKDList(this)

        else -> {
            val (dimension, partition) = partitionPolicy(this, bounds) ?: return DoubleKDList(
                this
            )

            val (less, greater) = this.partition { it[dimension] < partition }

            val (lowerBounds, upperBounds) = bounds.split(dimension, partition)

            return DoubleKDNode(
                dimension = dimension,
                partition = partition,
                less = less.buildKdTree(lowerBounds, partitionPolicy),
                greater = greater.buildKdTree(upperBounds, partitionPolicy)
            )
        }
    }

}
