package tomasvolker.kloudpoint.algorithms.kdtree

import tomasvolker.kloudpoint.math.DoubleVector3
import tomasvolker.kloudpoint.math.doubleset.closedRange
import tomasvolker.kloudpoint.math.doubleset.max
import tomasvolker.kloudpoint.math.doubleset.min
import tomasvolker.kloudpoint.model.*
import tomasvolker.kloudpoint.model.primitives.Point
import tomasvolker.numeriko.core.interfaces.array1d.double.DoubleArray1D
import tomasvolker.numeriko.core.primitives.squared
import kotlin.math.max

fun DoubleKDTree<*>.toIndentedString(indent: Int = 0): String = "|".repeat(indent) + when(this) {
    is DoubleKDList -> "-${values.size} values\n"
    is DoubleKDNode -> "-dim: $dimension less: $partition\n" + less.toIndentedString(indent+1) +
            "|".repeat(indent) + "-dim: $dimension greater: $partition\n" + greater.toIndentedString(indent+1)
}

fun <V: DoubleArray1D> DoubleKDTree<V>.leafList(): List<List<V>> = when(this) {
    is DoubleKDList<V> -> listOf(values)
    is DoubleKDNode<V> -> less.leafList() + greater.leafList()
}

fun DoubleKDTree<*>.leafCount(): Int = when(this) {
    is DoubleKDList<*> -> 1
    is DoubleKDNode<*> -> less.leafCount() + greater.leafCount()
}

fun DoubleKDTree<*>.pointCount(): Int = when(this) {
    is DoubleKDList<*> -> values.size
    is DoubleKDNode<*> -> less.pointCount() + greater.pointCount()
}

fun DoubleKDTree<*>.nodeCount(): Int = when(this) {
    is DoubleKDList<*> -> 0
    is DoubleKDNode<*> -> 1 + less.nodeCount() + greater.nodeCount()
}

fun DoubleKDTree<*>.emptyLeafs(): Int = when(this) {
    is DoubleKDList<*> -> if (values.isEmpty()) 1 else 0
    is DoubleKDNode<*> -> less.emptyLeafs() + greater.emptyLeafs()
}

fun DoubleKDTree<*>.depth(): Int = when(this) {
    is DoubleKDList<*> -> 0
    is DoubleKDNode<*> -> 1 + max(less.depth(), greater.depth())
}

fun DoubleKDTree<DoubleVector3>.pointsInBounds(
    boundingBox: BoundingBox3D
): List<Point> {

    if (boundingBox.isEmpty) return emptyList()

    return when(this) {
        is DoubleKDList<DoubleVector3> -> values.filter { it in boundingBox }
        is DoubleKDNode<DoubleVector3> -> {
            val range = boundingBox.range(dimension)
            when {
                partition < range.min -> greater.pointsInBounds(boundingBox)
                range.max < partition -> less.pointsInBounds(boundingBox)
                else -> less.pointsInBounds(boundingBox) + greater.pointsInBounds(boundingBox)
            }
        }
    }
}

fun DoubleKDTree<DoubleVector3>.pointsAround(
    point: DoubleVector3,
    radius: Double
): List<Point> {

    val inBox = pointsInBounds(
        BoundingBox3D(
            listOf(
                closedRange(point.x - radius, point.x + radius),
                closedRange(point.y - radius, point.y + radius),
                closedRange(point.z - radius, point.z + radius)
            )
        )
    )

    return inBox.filter { distanceLessThan(it, point, radius) }
}

fun distanceLessThan(point1: DoubleVector3, point2: DoubleVector3, distance: Double): Boolean =
    (point1.x - point2.x).squared() + (point1.y - point2.y).squared() + (point1.z - point2.z).squared() < distance.squared()

fun DoubleKDTree<*>.printStats() {

    println("""
        Point count: ${pointCount()}
        Leaf count: ${leafCount()}
        Node count: ${nodeCount()}
        Depth: ${depth()}
        Empty Leafs: ${emptyLeafs()}
    """.trimIndent())

}