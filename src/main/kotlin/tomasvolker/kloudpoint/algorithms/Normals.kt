package tomasvolker.kloudpoint.algorithms

import com.github.tomasvolker.parallel.mapParallel
import tomasvolker.kloudpoint.algorithms.kdtree.DoubleKDTree
import tomasvolker.kloudpoint.algorithms.kdtree.buildKdTree
import tomasvolker.kloudpoint.algorithms.kdtree.pointsAround
import tomasvolker.kloudpoint.math.V3
import tomasvolker.kloudpoint.model.LocalDescriptor
import tomasvolker.kloudpoint.model.PointCloud
import tomasvolker.kloudpoint.model.computeCentroid
import tomasvolker.kloudpoint.model.primitives.Point
import tomasvolker.kloudpoint.model.primitives.Vector3
import tomasvolker.numeriko.core.functions.applyDiv
import tomasvolker.numeriko.core.interfaces.array2d.double.DoubleArray2D
import tomasvolker.numeriko.core.interfaces.array2d.generic.forEachIndex
import tomasvolker.numeriko.core.interfaces.factory.doubleZeros

fun PointCloud.covarianceMatrix(
    center: Point = computeCentroid()
): DoubleArray2D {

    val result = doubleZeros(3, 3).asMutable()

    for (point in this) {

        result.forEachIndex { i0, i1 ->
            result[i0, i1] += (point[i0]-center[i0]) * (point[i1]-center[i1])
        }

    }

    result.applyDiv(this.size)

    return result
}

fun PointCloud.estimateNormal(): Vector3 {

    val decomposition = covarianceMatrix().eigenDecomposition(symetric = true)

    return decomposition.eigenVectorList.minBy { it.value }?.vector?.toDoubleVector3() ?: error("")
}


private fun PointCloud.estimateNormal(
    radius: Double,
    position: Point,
    tree: DoubleKDTree<Point> = buildKdTree()
): LocalDescriptor<Vector3> =
    LocalDescriptor(
        point = position,
        feature = tree.pointsAround(position, radius = radius)
                    .estimateNormal()
                    .let { it * (it inner V3[1, 1, 1]) }
                    .normalized()
    )


fun PointCloud.estimateNormals(
    radius: Double = 0.2,
    positions: PointCloud = this,
    tree: DoubleKDTree<Point> = buildKdTree(),
    parallelizationSize: Int = 1000
): List<LocalDescriptor<Vector3>> =
    positions
        .mapParallel(parallelizationSize) {
            estimateNormal(
                radius = radius,
                position = it,
                tree = tree
            )
        }

