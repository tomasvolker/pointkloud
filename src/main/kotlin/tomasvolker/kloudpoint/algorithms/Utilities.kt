package tomasvolker.kloudpoint.algorithms

import org.ejml.data.DMatrixRMaj
import tomasvolker.kloudpoint.math.DoubleVector3
import tomasvolker.kloudpoint.math.V3
import tomasvolker.kloudpoint.model.primitives.Vector3
import tomasvolker.numeriko.core.functions.applyPlus
import tomasvolker.numeriko.core.functions.inner
import tomasvolker.numeriko.core.functions.norm2
import tomasvolker.numeriko.core.index.IndexProgression
import tomasvolker.numeriko.core.interfaces.array1d.double.DoubleArray1D
import tomasvolker.numeriko.core.interfaces.array1d.double.MutableDoubleArray1D
import tomasvolker.numeriko.core.interfaces.array2d.double.DoubleArray2D
import tomasvolker.numeriko.core.interfaces.array2d.double.MutableDoubleArray2D
import tomasvolker.numeriko.core.interfaces.array2d.generic.forEachIndex
import kotlin.math.cosh

inline fun sumVector(indices: IntProgression, value: (i: Int)-> DoubleArray1D): DoubleArray1D {
    var result: MutableDoubleArray1D? = null
    for (i in indices) {
        result = if (result == null)
            value(i).copy().asMutable()
        else
            result.applyPlus(value(i))
    }
    return result ?: throw IllegalArgumentException()
}


infix fun DoubleArray1D.colinearityFactor(other: DoubleArray1D): Double =
    (this inner other) / (this.norm2() * other.norm2())

fun DoubleArray2D.toEjmlMatrix(): DMatrixRMaj {
    val mat = DMatrixRMaj(shape0, shape1)
    forEachIndex { i0, i1 ->
        mat.set(i0, i1, this[i0, i1])
    }
    return mat
}

fun DMatrixRMaj.toDoubleArray2D(): DoubleArray2D =
    tomasvolker.numeriko.core.interfaces.factory.doubleArray2D(numRows, numCols) { i0, i1 ->
        this[i0, i1]
    }

fun DMatrixRMaj.toDoubleArray1D(): DoubleArray1D =
    tomasvolker.numeriko.core.interfaces.factory.doubleArray1D(numRows) { i ->
        this[i]
    }

fun DoubleArray1D.asDoubleVector3() = this as DoubleVector3
fun DoubleArray1D.toDoubleVector3(): Vector3 {
    require(this.size == 3)
    return V3[this[0], this[1], this[2]]
}