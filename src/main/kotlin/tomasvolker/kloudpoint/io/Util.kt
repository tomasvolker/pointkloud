package tomasvolker.kloudpoint.io

import tomasvolker.kloudpoint.model.Vertex
import java.io.DataInputStream
import java.io.File
import java.io.InputStream
import java.lang.IllegalArgumentException

fun parseError(message: String): Nothing = throw IllegalArgumentException(message)

fun DataInputStream.readVertex(): Vertex =
    Vertex(
        x = readFloat(),
        y = readFloat(),
        z = readFloat(),
        reflectance = readFloat(),
        label = readInt(),
        clazz = readInt()
    )

fun InputStream.dataStream(): DataInputStream = DataInputStream(this)

fun File.dataStream(): DataInputStream = inputStream().dataStream()

fun File.bufferedDataStream(bufferSize: Int = DEFAULT_BUFFER_SIZE): DataInputStream =
    inputStream().buffered(bufferSize).dataStream()

inline fun InputStream.readAsciiLinesUntil(predicate: (String)->Boolean): List<String> {
    val result = mutableListOf<String>()
    do {
        val line = readAsciiLine() ?: parseError("Illegal header")
        result.add(line)
    } while (!predicate(line))

    return result
}

fun InputStream.readAsciiLine(): String? {

    val builder = StringBuilder()

    do {

        val value = read()
        if(value == -1) return null

        val char = value.toChar()
        builder.append(char)

    } while (char != '\n')

    return builder.toString()
}