package tomasvolker.kloudpoint.io

import tomasvolker.kloudpoint.math.V3
import tomasvolker.kloudpoint.model.*
import tomasvolker.kloudpoint.model.primitives.Point
import java.io.File
import java.io.InputStream

/*

ply
format binary_big_endian 1.0
comment CMM-ARMINES: MorphoViewer3D - PLY File
obj_info Points and polygons
element vertex 10000000
property float32 x
property float32 y
property float32 z
property float32 reflectance
property uint label
property uint class
end_header

 */

fun loadRueMadame(file: File): List<CloudObject> {

    val objectMap = mutableMapOf<Int, MutableList<Point>>()
    val categoryMap = mutableMapOf<Int, ObjectCategory>()

    file.bufferedDataStream().use { stream ->

        with(stream) {

            val count = readRueMadameHeader()

            repeat(count) { i ->

                val x = readFloat()
                val y = readFloat()
                val z = readFloat()
                val reflectance = readFloat()
                val label = readInt()
                val clazz = readInt()

                objectMap.getOrPut(label) { mutableListOf() }.add(V3[x, y, z])
                categoryMap[label] = ObjectCategory.fromValue(clazz)

            }


        }

    }

    return objectMap.asSequence().map {
        CloudObject(
            identifier = it.key,
            category = categoryMap[it.key] ?: ObjectCategory.NOISE,
            pointCloud = it.value
        )
    }.toList()
}




fun InputStream.readRueMadameHeader(): Int {

    val magicNumber = ByteArray(4)
    if(read(magicNumber) != 4) parseError("No ply header")
    val ply = magicNumber.toString(Charsets.US_ASCII)

    if(ply != "ply\n") parseError("No ply header")

    val header = readAsciiLinesUntil { it == "end_header\n" }
        .map { it.removeSuffix("\n") }
        .filterNot { it.startsWith("comment") || it.startsWith("obj_info") }

    val (_, format, _) = header.first().split(" ")

    if (format != "binary_big_endian") parseError("format $format is not supported")

    val (_, _, count) = header[1].split(" ")

    return count.toInt()
}