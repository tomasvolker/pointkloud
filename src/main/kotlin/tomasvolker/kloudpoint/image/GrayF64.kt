package tomasvolker.kloudpoint.image

import boofcv.struct.image.GrayF64
import tomasvolker.koofcv.constructor.grayF32
import tomasvolker.koofcv.interation.forEachIndex
import tomasvolker.koofcv.interation.forEachIndexXY

inline fun GrayF64.elementWise(
    destination: GrayF64? = null,
    operation: (Double)->Double
): GrayF64 {
    val result = destination ?: createSameShape()
    forEachIndex { index ->
        result.data[index] = operation(data[index])
    }
    return result
}

inline fun GrayF64.elementWiseIndexed(
    destination: GrayF64? = null,
    operation: (x: Int, y: Int, value: Double)->Double
): GrayF64 {
    val result = destination ?: createSameShape()
    forEachIndexXY { index, x, y ->
        result.data[index] = operation(x, y, data[index])
    }
    return result
}

inline fun GrayF64.mask(
    destination: GrayF64? = null,
    predicate: (x: Int, y: Int)->Boolean
) = elementWiseIndexed(destination) { x, y, value ->
    if (predicate(x, y)) value else 0.0
}

inline fun GrayF64.elementWise(
    other: GrayF64,
    destination: GrayF64? = null,
    operation: (Double, Double)->Double
): GrayF64 {
    require(this.width == other.width)
    require(this.height == other.height)

    val result = destination ?: createSameShape()
    forEachIndex { index ->
        result.data[index] = operation(this.data[index], other.data[index])
    }
    return result
}

operator fun GrayF64.plus(other: GrayF64): GrayF64 =
        this.elementWise(other) { t, o -> t + o }

operator fun GrayF64.minus(other: GrayF64): GrayF64 =
    this.elementWise(other) { t, o -> t - o }

operator fun GrayF64.times(other: GrayF64): GrayF64 =
    this.elementWise(other) { t, o -> t * o }

operator fun GrayF64.plus(other: Double): GrayF64 =
    this.elementWise { it + other }

operator fun GrayF64.minus(other: Double): GrayF64 =
    this.elementWise { it - other }

operator fun GrayF64.times(other: Double): GrayF64 =
    this.elementWise { it * other }

fun GrayF64.toGrayF32() = grayF32(width, height) { x, y ->
    this[x, y].toFloat()
}