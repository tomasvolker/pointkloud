package tomasvolker.kloudpoint.image

import boofcv.struct.image.GrayF32
import tomasvolker.koofcv.interation.forEachIndex
import tomasvolker.koofcv.interation.forEachIndexXY

inline fun GrayF32.elementWise(
    destination: GrayF32? = null,
    operation: (Float)->Float
): GrayF32 {
    val result = destination ?: createSameShape()
    forEachIndex { index ->
        result.data[index] = operation(data[index])
    }
    return result
}

inline fun GrayF32.elementWiseIndexed(
    destination: GrayF32? = null,
    operation: (x: Int, y: Int, value: Float)->Float
): GrayF32 {
    val result = destination ?: createSameShape()
    forEachIndexXY { index, x, y ->
        result.data[index] = operation(x, y, data[index])
    }
    return result
}

inline fun GrayF32.mask(
    destination: GrayF32? = null,
    predicate: (x: Int, y: Int)->Boolean
) = elementWiseIndexed(destination) { x, y, value ->
    if (predicate(x, y)) value else 0.0f
}

inline fun GrayF32.elementWise(
    other: GrayF32,
    destination: GrayF32? = null,
    operation: (Float, Float)->Float
): GrayF32 {
    require(this.width == other.width)
    require(this.height == other.height)

    val result = destination ?: createSameShape()
    forEachIndex { index ->
        result.data[index] = operation(this.data[index], other.data[index])
    }
    return result
}

operator fun GrayF32.plus(other: GrayF32): GrayF32 =
        this.elementWise(other) { t, o -> t + o }

operator fun GrayF32.minus(other: GrayF32): GrayF32 =
    this.elementWise(other) { t, o -> t - o }

operator fun GrayF32.times(other: GrayF32): GrayF32 =
    this.elementWise(other) { t, o -> t * o }

operator fun GrayF32.plus(other: Float): GrayF32 =
    this.elementWise { it + other }

operator fun GrayF32.minus(other: Float): GrayF32 =
    this.elementWise { it - other }

operator fun GrayF32.times(other: Float): GrayF32 =
    this.elementWise { it * other }
