package tomasvolker.kloudpoint.image

import boofcv.struct.image.ImageBase
import tomasvolker.kloudpoint.model.IntPoint2

fun ImageBase<*>.isInBorder(x: Int, y: Int): Boolean =
        x == 0 || x == width-1 || y == 0 || y == height-1

fun ImageBase<*>.isInBorder(point: IntPoint2): Boolean =
    isInBorder(point.x, point.y)

