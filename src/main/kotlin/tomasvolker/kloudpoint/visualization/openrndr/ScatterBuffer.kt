package tomasvolker.kloudpoint.visualization.openrndr

import org.openrndr.draw.*
import tomasvolker.kloudpoint.model.primitives.Point
import tomasvolker.kloudpoint.visualization.model.Scatter3D
import tomasvolker.kloudpoint.visualization.nextColorRGB
import tomasvolker.kloudpoint.visualization.write
import kotlin.random.Random

class ScatterBuffer(
    val drawing: Scatter3D
): VertexBufferDrawable {

    override val vertexBuffer = vertexBuffer(
        vertexFormat {
            position(3)
            color(4)
        },
        drawing.pointList.size
    ).apply {

        val mapper = drawing.colorMapper ?: Random.nextColorRGB().let { color ->
            { p: Point -> color }
        }

        put {
            drawing.pointList.forEach {
                write(it)
                write(mapper(it))
            }
        }
    }

    override fun draw(drawer: Drawer) {

        drawer.isolated {
            shadeStyle = shadeStyle {
                fragmentTransform = """x_fill.rgba = va_color;"""
            }
            vertexBuffer(vertexBuffer, DrawPrimitive.POINTS)
        }

    }

}