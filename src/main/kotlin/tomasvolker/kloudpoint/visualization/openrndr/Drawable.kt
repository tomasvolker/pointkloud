package tomasvolker.kloudpoint.visualization.openrndr

import org.openrndr.draw.ColorBuffer
import org.openrndr.draw.Drawer
import org.openrndr.draw.VertexBuffer

interface Drawable {
    fun draw(drawer: Drawer)
}

interface VertexBufferDrawable: Drawable, AutoCloseable {

    val vertexBuffer: VertexBuffer

    override fun close() {
        vertexBuffer.destroy()
    }

}

interface ColorBufferDrawable: Drawable, AutoCloseable {

    val colorBuffer: ColorBuffer

    override fun close() {
        colorBuffer.destroy()
    }

}