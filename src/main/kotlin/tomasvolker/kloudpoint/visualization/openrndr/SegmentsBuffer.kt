package tomasvolker.kloudpoint.visualization.openrndr

import org.openrndr.draw.*
import tomasvolker.kloudpoint.model.primitives.Point
import tomasvolker.kloudpoint.visualization.model.Segments3D
import tomasvolker.kloudpoint.visualization.nextColorRGB
import tomasvolker.kloudpoint.visualization.write
import kotlin.random.Random

class SegmentsBuffer(
    val drawing: Segments3D
): VertexBufferDrawable {

    override val vertexBuffer = vertexBuffer(
        vertexFormat {
            position(3)
            color(4)
        },
        drawing.segmentList.size * 2
    ).apply {

        val mapper = drawing.colorMapper ?: Random.nextColorRGB().let { color ->
            { p: Point -> color }
        }

        put {
            drawing.segmentList.forEach {
                write(it.start)
                write(mapper(it.start))
                write(it.end)
                write(mapper(it.end))
            }
        }
    }

    override fun draw(drawer: Drawer) {
        drawer.isolated {
            shadeStyle = shadeStyle {
                fragmentTransform = """x_fill.rgba = va_color;"""
            }
            vertexBuffer(vertexBuffer, DrawPrimitive.LINES)
        }
    }

}