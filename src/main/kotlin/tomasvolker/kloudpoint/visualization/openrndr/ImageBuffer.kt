package tomasvolker.kloudpoint.visualization.openrndr


import org.openrndr.color.ColorRGBa
import org.openrndr.draw.*
import org.openrndr.math.transforms.rotateX
import tomasvolker.kloudpoint.visualization.image
import tomasvolker.kloudpoint.visualization.model.Image
import tomasvolker.kloudpoint.visualization.showPlot3D
import tomasvolker.koofcv.constructor.grayF64
import tomasvolker.koofcv.interation.forEachIndex

class ImageBuffer(
    val image: Image
): ColorBufferDrawable {

    val gray =  image.image

    override val colorBuffer = colorBuffer(
        width = gray.width,
        height = gray.height,
        format = ColorFormat.R,
        type = ColorType.FLOAT32
    ).apply {

        shadow.buffer.rewind()

        gray.forEachIndex { index ->
            shadow.buffer.putFloat(gray.data[index].toFloat())
        }

        shadow.upload()

    }

    override fun draw(drawer: Drawer) {
        drawer.isolated {
            model = image.position
            fill = ColorRGBa.WHITE
            shadeStyle = shadeStyle {
                fragmentTransform = """x_fill.rgba = vec4(x_fill[0], x_fill[0], x_fill[0], 1.0);"""
            }
            drawer.image(colorBuffer)
        }
    }

}

fun main() {

    showPlot3D {

        image {
            image = grayF64(10, 10) { x, y -> (x + y) / 20.0 }
            position = rotateX(90.0)
        }

    }

}