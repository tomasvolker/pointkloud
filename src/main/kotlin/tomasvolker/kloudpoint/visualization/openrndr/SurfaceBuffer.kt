package tomasvolker.kloudpoint.visualization.openrndr

import org.openrndr.color.ColorHSLa
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.*
import org.openrndr.math.Vector3
import tomasvolker.kloudpoint.model.primitives.Point
import tomasvolker.kloudpoint.model.x
import tomasvolker.kloudpoint.model.y
import tomasvolker.kloudpoint.model.z
import tomasvolker.kloudpoint.visualization.model.CameraType
import tomasvolker.kloudpoint.visualization.model.Surface3D
import tomasvolker.kloudpoint.visualization.nextColorRGB
import tomasvolker.kloudpoint.visualization.showPlot3D
import tomasvolker.kloudpoint.visualization.surface
import tomasvolker.kloudpoint.visualization.write
import tomasvolker.numeriko.core.interfaces.array2d.generic.get
import tomasvolker.numeriko.core.interfaces.factory.array2D
import tomasvolker.numeriko.core.primitives.squared
import java.nio.ByteBuffer
import java.nio.ByteOrder
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.hypot
import kotlin.math.sin
import kotlin.random.Random

class SurfaceBuffer(
    val drawing: Surface3D
): VertexBufferDrawable {

    val pointGrid = drawing.pointGrid

    val triangleCount get() = 2 * (pointGrid.shape0-1) * (pointGrid.shape1-1)
    val segmentCount get() = (pointGrid.shape0-1) * pointGrid.shape1 + pointGrid.shape0 * (pointGrid.shape1-1)

    override val vertexBuffer = vertexBuffer(
        vertexFormat {
            position(3)
            color(4)
        },
        drawing.pointGrid.size
    ).apply {

        val mapper = drawing.colorMapper ?: Random.nextColorRGB().let { color ->
            { p: Point -> color }
        }

        put {

            for (i1 in 0 until pointGrid.shape1) {
                for (i0 in 0 until pointGrid.shape0) {
                    val point = pointGrid[i0, i1]
                    write(point)
                    write(mapper(point))
                }
            }

        }
    }

    var vertexIndexBuffer: IndexBuffer = indexBuffer(3 * triangleCount, IndexType.INT32).apply {

        val temp = ByteBuffer.allocateDirect(3 * triangleCount * 4).order(ByteOrder.nativeOrder())
        val intTemp = temp.asIntBuffer()

        for (i1 in 0 until pointGrid.shape1-1) {
            for (i0 in 0 until pointGrid.shape0-1) {

                intTemp.put(toLinear(i0, i1))
                intTemp.put(toLinear(i0+1, i1))
                intTemp.put(toLinear(i0, i1+1))

                intTemp.put(toLinear(i0+1, i1))
                intTemp.put(toLinear(i0+1, i1+1))
                intTemp.put(toLinear(i0, i1+1))

            }
        }

        intTemp.flip()

        write(temp)

    }

    var wireframeIndexBuffer: IndexBuffer = indexBuffer(2 * segmentCount, IndexType.INT32).apply {

        val temp = ByteBuffer.allocateDirect(2 * segmentCount * 4).order(ByteOrder.nativeOrder())
        val intTemp = temp.asIntBuffer()

        for (i1 in 0 until pointGrid.shape1-1) {
            for (i0 in 0 until pointGrid.shape0) {
                intTemp.put(toLinear(i0, i1))
                intTemp.put(toLinear(i0, i1+1))
            }
        }

        for (i0 in 0 until pointGrid.shape0-1) {
            for (i1 in 0 until pointGrid.shape1) {
                intTemp.put(toLinear(i0, i1))
                intTemp.put(toLinear(i0+1, i1))
            }
        }

        intTemp.flip()

        write(temp)

    }

    fun toLinear(i0: Int, i1: Int) = i1 * pointGrid.shape0 + i0

    override fun draw(drawer: Drawer) {

        drawer.isolated {

            shadeStyle = shadeStyle {
                fragmentTransform = """x_fill.rgba = va_color;"""
            }
            drawer.vertexBuffer(vertexIndexBuffer, listOf(vertexBuffer), DrawPrimitive.TRIANGLES)

            val viewDirection = view.inversed * Vector3.UNIT_Z
            translate(viewDirection * 0.01)
            shadeStyle = null
            fill = ColorRGBa.BLACK
            drawer.vertexBuffer(wireframeIndexBuffer, listOf(vertexBuffer), DrawPrimitive.LINES)


        }

    }

}


fun main() {

    showPlot3D {

        camera = CameraType.ORBITAL
        fullscreen = true
        /*
        surface {

            colorMapper = { ColorHSLa(it.z * 360, 1.0, 0.5).toRGBa() }

            pointGrid = array2D(101, 101) { u, v ->
                val phi = PI * u / 100.0
                val theta = 2 * PI * v / 100.0
                Point(
                    sin(phi) * cos(theta),
                    sin(phi) * sin(theta),
                    cos(phi)
                )
            }

        }
        */

        surface {

            colorMapper = { ColorHSLa(it.z * 360, 1.0, 0.5).toRGBa() }

            pointGrid = array2D(101, 101) { u, v ->
                val phi = 2 * PI * u / 100.0
                val theta = 2 * PI * v / 100.0
                Point(
                    (3.0 + cos(phi)) * cos(theta),
                    (3.0 + cos(phi)) * sin(theta),
                    sin(phi)
                )
            }

        }

     /*
        surface {

            colorMapper = { ColorHSLa(hypot(it.x, it.y) * 360, 1.0, 0.5).toRGBa() }

            pointGrid = array2D(100, 100) { u, v ->
                Point(
                    (u - 50) / 10.0,
                    (v - 50) / 10.0,
                    ((u - 50).squared() - (v - 50).squared()) / 500.0
                )
            }

        }
*/
/*
        surface {

            colorMapper = { ColorHSLa(it.z * 360, 1.0, 0.5).toRGBa() }

            pointGrid = array2D(21, 51) { u, v ->
                val x = (u / 20.0 - 0.5)
                val theta = 2 * PI * v / 50.0
                Point(
                    (x * cos(theta * 0.5) + 1.0) * cos(theta),
                    (x * cos(theta  * 0.5) + 1.0) * sin(theta),
                    x * sin(theta * 0.5)
                )
            }

        }
*/
    }


}