package tomasvolker.kloudpoint.visualization

import boofcv.struct.image.GrayF32
import boofcv.struct.image.GrayF64
import boofcv.struct.image.GrayU8
import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.colorBuffer
import org.openrndr.extensions.Screenshots
import tomasvolker.openrndr.math.extensions.CursorPosition
import tomasvolker.openrndr.math.extensions.Grid2D
import tomasvolker.openrndr.math.extensions.PanZoom
import java.awt.image.BufferedImage

fun showImageGray(image: GrayF32) {

    application {

        configure {
            width = image.width
            height = image.height
            windowResizable = true
        }

        program {

            backgroundColor = ColorRGBa.WHITE
            extend(CursorPosition())
            extend(PanZoom())
            extend(Grid2D())

            val buffer = colorBuffer(image.width, image.height)
            buffer.write(image)

            extend {
                drawer.image(buffer)
            }

        }

    }

}

fun showImageGray(image: GrayF64) {

    application {

        configure {
            width = image.width
            height = image.height
            windowResizable = true
        }

        program {

            backgroundColor = ColorRGBa.WHITE
            extend(CursorPosition())
            extend(PanZoom())
            extend(Grid2D())
            extend(Screenshots())

            val buffer = colorBuffer(image.width, image.height)
            buffer.write(image)

            extend {
                drawer.image(buffer)
            }

        }

    }

}

fun showImageBinary(image: GrayU8) {

    application {

        configure {
            width = image.width
            height = image.height
            windowResizable = true
        }

        program {

            backgroundColor = ColorRGBa.BLACK
            extend(CursorPosition())
            extend(PanZoom())
            extend(Grid2D())

            val buffer = colorBuffer(image.width, image.height)
            buffer.writeBinary(image)

            extend {
                drawer.image(buffer)
            }

        }

    }

}

fun showImage(image: BufferedImage, background: ColorRGBa = ColorRGBa.WHITE) {

    application {

        configure {
            width = image.width
            height = image.height
            windowResizable = true
        }

        program {

            backgroundColor = background
            extend(CursorPosition())
            extend(PanZoom())
            extend(Grid2D())
            extend(Screenshots())

            val buffer = colorBuffer(image.width, image.height)
            buffer.write(image)

            extend {
                drawer.image(buffer)
            }

        }

    }

}