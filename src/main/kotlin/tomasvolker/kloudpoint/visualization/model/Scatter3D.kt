package tomasvolker.kloudpoint.visualization.model

import org.openrndr.color.ColorRGBa
import org.openrndr.draw.DrawPrimitive
import org.openrndr.draw.Drawer
import org.openrndr.draw.shadeStyle
import tomasvolker.kloudpoint.model.primitives.Point

data class Scatter3D(
    val pointList: List<Point> = emptyList(),
    val colorMapper: ColorMapper? = null
): Drawing3D {

    @PlotMarker
    data class Builder(
        var pointList: List<Point> = emptyList(),
        var colorMapper: ColorMapper? = null
    ) {

        fun build(): Scatter3D =
            Scatter3D(
                    pointList = pointList,
                    colorMapper = colorMapper
                )

    }

}

var Scatter3D.Builder.color: ColorRGBa?
        get() = null
        set(value) { value?.let { colorMapper = { value } } }
