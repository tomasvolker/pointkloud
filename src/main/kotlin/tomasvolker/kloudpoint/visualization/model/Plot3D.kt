package tomasvolker.kloudpoint.visualization.model

import org.openrndr.color.ColorRGBa
import tomasvolker.kloudpoint.math.V3
import tomasvolker.kloudpoint.model.primitives.Point

enum class CameraType {
    ORBITAL,
    FIRST_PERSON
}

data class Plot3D(
    val title: String = "",
    val camera: CameraType = CameraType.ORBITAL,
    val fullscreen: Boolean = false,
    val backgroundColor: ColorRGBa = ColorRGBa.BLACK,
    val drawingList: List<Drawing3D> = emptyList(),
    val initialPosition: Point = V3.ZERO
) {

    @PlotMarker
    data class Builder(
        var title: String = "",
        var camera: CameraType = CameraType.ORBITAL,
        var fullscreen: Boolean = false,
        var backgroundColor: ColorRGBa = ColorRGBa.BLACK,
        var drawingList: MutableList<Drawing3D> = mutableListOf(),
        var initialPosition: Point = V3.ZERO
    ) {

        fun build(): Plot3D =
                Plot3D(
                    title = title,
                    camera = camera,
                    fullscreen = fullscreen,
                    backgroundColor = backgroundColor,
                    drawingList = drawingList,
                    initialPosition = initialPosition
                )

    }

}
