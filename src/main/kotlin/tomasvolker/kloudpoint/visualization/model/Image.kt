package tomasvolker.kloudpoint.visualization.model

import boofcv.struct.image.GrayF64
import org.openrndr.math.Matrix44

data class Image(
    val image: GrayF64 = GrayF64(0, 0),
    val position: Matrix44 = Matrix44.IDENTITY
): Drawing3D {

    @PlotMarker
    data class Builder(
        var image: GrayF64 = GrayF64(0, 0),
        var position: Matrix44 = Matrix44.IDENTITY
    ) {

        fun build(): Image =
            Image(
                image = image,
                position = position
            )

    }

}
