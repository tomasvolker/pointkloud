package tomasvolker.kloudpoint.visualization.model

import org.openrndr.color.ColorRGBa
import org.openrndr.draw.DrawPrimitive
import org.openrndr.draw.Drawer
import org.openrndr.draw.shadeStyle
import org.openrndr.math.Vector3
import tomasvolker.kloudpoint.math.DoubleVector3
import tomasvolker.kloudpoint.model.primitives.Point
import tomasvolker.numeriko.core.interfaces.array2d.generic.Array2D
import tomasvolker.numeriko.core.interfaces.factory.array2D
import tomasvolker.numeriko.core.interfaces.factory.doubleArray2D

data class Surface3D(
    val pointGrid: Array2D<Point> = array2D(0, 0) { _, _ -> Point(0.0, 0.0, 0.0) },
    val colorMapper: ColorMapper? = null
): Drawing3D {

    @PlotMarker
    data class Builder(
        var pointGrid: Array2D<Point> = array2D(0, 0) { _, _ -> Point(0.0, 0.0, 0.0) },
        var colorMapper: ColorMapper? = null
    ) {

        fun build(): Surface3D =
            Surface3D(
                pointGrid = pointGrid,
                colorMapper = colorMapper
            )

    }

}

var Surface3D.Builder.color: ColorRGBa?
        get() = null
        set(value) { value?.let { colorMapper = { value } } }
