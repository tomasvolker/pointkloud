package tomasvolker.kloudpoint.visualization.model

import org.openrndr.color.ColorRGBa
import org.openrndr.draw.Drawer
import tomasvolker.kloudpoint.model.primitives.Point

interface Drawing3D

typealias ColorMapper = (Point)-> ColorRGBa
