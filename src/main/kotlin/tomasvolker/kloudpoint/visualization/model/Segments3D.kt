package tomasvolker.kloudpoint.visualization.model

import org.openrndr.color.ColorRGBa
import tomasvolker.kloudpoint.math.DoubleVector3
import tomasvolker.kloudpoint.model.primitives.Point
import tomasvolker.kloudpoint.model.primitives.Segment

data class Segments3D(
    val segmentList: List<Segment> = emptyList(),
    val colorMapper: ColorMapper? = null
): Drawing3D {

    @PlotMarker
    data class Builder(
        var segmentList: List<Segment> = emptyList(),
        var colorMapper: ColorMapper? = null
    ) {

        fun build(): Segments3D =
            Segments3D(
                segmentList = segmentList,
                colorMapper = colorMapper
            )

    }

}

var Segments3D.Builder.color: ColorRGBa?
        get() = null
        set(value) { value?.let { colorMapper = { value } } }
