package tomasvolker.kloudpoint.visualization

import boofcv.struct.image.GrayF64
import org.openrndr.Fullscreen
import org.openrndr.KEY_ESCAPE
import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.*
import org.openrndr.extensions.Screenshots
import org.openrndr.extras.camera.Debug3D
import org.openrndr.math.transforms.rotateX
import tomasvolker.kloudpoint.model.primitives.Point
import tomasvolker.kloudpoint.model.primitives.Segment
import tomasvolker.kloudpoint.visualization.model.*
import tomasvolker.kloudpoint.visualization.openrndr.*
import tomasvolker.numeriko.core.interfaces.array2d.generic.Array2D
import tomasvolker.numeriko.core.interfaces.factory.array2D
import tomasvolker.openrndr.math.extensions.FPSDisplay
import tomasvolker.openrndr.math.extensions.FirstPerson
import tomasvolker.openrndr.math.primitives.d

inline fun showPlot3D(title: String = "", init: Plot3D.Builder.()->Unit) {
    show(plot3D(title, init))
}

fun show(plot: Plot3D) {

    application {

        configure {
            width = 800
            height = 600
            windowResizable = true

            fullscreen = if (plot.fullscreen) Fullscreen.SET_DISPLAY_MODE else Fullscreen.DISABLED
            hideCursor = plot.fullscreen

        }

        program {

            val bufferList = plot.drawingList.map { drawing ->
                when(drawing) {
                    is Scatter3D -> ScatterBuffer(drawing)
                    is Segments3D -> SegmentsBuffer(drawing)
                    is Surface3D -> SurfaceBuffer(drawing)
                    is Image -> ImageBuffer(drawing) as Drawable
                    else -> error("drawing not supported")
                }
            }

            backgroundColor = ColorRGBa.BLACK

            keyboard.keyDown.listen {
                if (it.key == KEY_ESCAPE) application.exit()
            }

            when(plot.camera) {
                CameraType.ORBITAL -> extend(Debug3D()) {
                    orbitalCamera.panTo(rotateX(-90.0) * plot.initialPosition.toVector3())
                }
                CameraType.FIRST_PERSON -> extend(FirstPerson()) {
                    firstPersonCamera.moveTo(
                        plot.initialPosition.toVector3()
                    )
                }
            }

            extend(FPSDisplay())

            extend(Screenshots())

            extend {

                if (plot.camera == CameraType.ORBITAL)
                    drawer.view *= rotateX(-90.0)

                drawer.drawStyle.quality = DrawQuality.QUALITY
                drawer.depthWrite = true
                drawer.depthTestPass = DepthTestPass.LESS_OR_EQUAL

                bufferList.forEach { buffer ->
                    buffer.draw(drawer)
                }

            }

        }

    }

}

inline fun plot3D(title: String = "", init: Plot3D.Builder.()->Unit): Plot3D =
    Plot3D.Builder(title = title).apply(init).build()

inline fun Plot3D.Builder.scatter(
    pointCloud: List<Point> = emptyList(),
    init: Scatter3D.Builder.()->Unit
) {
    drawingList.add(Scatter3D.Builder(pointList = pointCloud).apply(init).build())
}

inline fun Plot3D.Builder.segments(
    segmentList: List<Segment> = emptyList(),
    init: Segments3D.Builder.()->Unit
) {
    drawingList.add(Segments3D.Builder(segmentList = segmentList).apply(init).build())
}

inline fun Plot3D.Builder.surface(
    segmentList: Array2D<Point> = array2D(0, 0) { _, _ -> Point(0.0, 0.0, 0.0) },
    init: Surface3D.Builder.()->Unit
) {
    drawingList.add(Surface3D.Builder(pointGrid = segmentList).apply(init).build())
}

inline fun Plot3D.Builder.image(
    image: GrayF64 = GrayF64(0, 0),
    init: Image.Builder.()->Unit
) {
    drawingList.add(Image.Builder(image = image).apply(init).build())
}
