package tomasvolker.kloudpoint.visualization

import org.openrndr.color.ColorHSLa
import org.openrndr.color.ColorRGBa

fun grayColorMapper(min: Double, max: Double): (Double)-> ColorRGBa {
    val range = max - min
    return { ColorRGBa.WHITE.shade((it - min) / range) }
}

fun heatColorMapper(min: Double, max: Double): (Double)-> ColorRGBa {
    val range = max - min
    return { ColorHSLa(240 * (max - it.coerceIn(min, max)) / range, 1.0, 0.5).toRGBa() }
}