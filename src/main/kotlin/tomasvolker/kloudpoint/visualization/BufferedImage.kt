package tomasvolker.kloudpoint.visualization

import org.openrndr.color.ColorRGBa
import org.openrndr.draw.ColorBuffer
import tomasvolker.kloudpoint.model.IntPoint2
import java.awt.image.BufferedImage
import kotlin.math.roundToInt

operator fun BufferedImage.get(x: Int, y: Int) =
    ColorRGBa.fromHex(getRGB(x, y))

operator fun BufferedImage.set(x: Int, y: Int, color: ColorRGBa) {
    setRGB(x, y, color.toHex())
}

operator fun BufferedImage.get(point: IntPoint2) = get(point.x, point.y)
operator fun BufferedImage.set(point: IntPoint2, color: ColorRGBa) = set(point.x, point.y, color)

fun ColorRGBa.toHex() =
    ((255 * a).roundToInt() shl 24) or
    ((255 * r).roundToInt() shl 16) or
    ((255 * g).roundToInt() shl 8) or
    (255 * b).roundToInt()

