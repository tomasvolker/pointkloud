package tomasvolker.kloudpoint.visualization

import org.openrndr.color.ColorRGBa
import org.openrndr.draw.BufferWriter
import org.openrndr.math.Vector3
import tomasvolker.kloudpoint.model.primitives.Point
import tomasvolker.kloudpoint.model.x
import tomasvolker.kloudpoint.model.y
import tomasvolker.kloudpoint.model.z
import kotlin.random.Random

fun Random.nextColorRGB() = ColorRGBa(nextDouble(), nextDouble(), nextDouble())
fun Point.toVector3() = Vector3(x, y, z)
fun BufferWriter.write(point: Point) {
    write(point.x.toFloat(), point.y.toFloat(), point.z.toFloat())
}