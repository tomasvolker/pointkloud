package tomasvolker.kloudpoint.visualization

import boofcv.struct.image.GrayF32
import boofcv.struct.image.GrayF64
import boofcv.struct.image.GrayU8
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.ColorBuffer
import tomasvolker.koofcv.interation.forEachPixel
import tomasvolker.koofcv.statistics.max
import tomasvolker.koofcv.statistics.min
import java.awt.image.BufferedImage

fun ColorBuffer.write(image: BufferedImage) {

    shadow.buffer.rewind()

    for (x in 0 until image.width) {
        for (y in 0 until image.height) {
            val renderedValue = image.getRGB(x, y)

            shadow.write(
                x,
                y,
                r = ((renderedValue and 0x00FF0000) ushr 16) / 255.0,
                g = ((renderedValue and 0x0000FF00) ushr 8) / 255.0,
                b = (renderedValue and 0x000000FF)/ 255.0,
                a = (((renderedValue and 0x00FFFFFF.inv()) ushr 24) / 255.0)
            )
        }
    }

    shadow.upload()
}

fun ColorBuffer.write(image: GrayF64) {

    val min = image.min()
    val range = image.max() - min

    shadow.buffer.rewind()
    image.forEachPixel { x, y, value ->

        val renderedValue = (value - min) / range

        shadow.write(
            x,
            y,
            renderedValue,
            renderedValue,
            renderedValue,
            1.0
        )
    }
    shadow.upload()
}

fun ColorBuffer.write(image: GrayF32) {

    val min = image.min()
    val range = image.max() - min

    shadow.buffer.rewind()
    image.forEachPixel { x, y, value ->

        val renderedValue = ((value - min) / range).toDouble()

        shadow.write(
            x,
            y,
            renderedValue,
            renderedValue,
            renderedValue,
            1.0
        )
    }
    shadow.upload()
}

fun ColorBuffer.writeBinary(image: GrayU8, colorRGBa: ColorRGBa = ColorRGBa.WHITE) {

    shadow.buffer.rewind()
    image.forEachPixel { x, y, value ->
        if (value != 0)
            shadow[x, y] = colorRGBa
    }
    shadow.upload()
}