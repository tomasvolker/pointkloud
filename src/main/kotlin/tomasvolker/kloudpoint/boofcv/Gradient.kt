package tomasvolker.kloudpoint.boofcv

import boofcv.alg.filter.derivative.DerivativeType
import boofcv.alg.filter.derivative.GImageDerivativeOps
import boofcv.core.image.border.BorderType
import boofcv.struct.image.GrayF32
import boofcv.struct.image.GrayF64

fun GrayF32.gradient(
    type: DerivativeType = DerivativeType.SOBEL,
    borderType: BorderType = BorderType.EXTENDED,
    gradientX: GrayF32? = null,
    gradientY: GrayF32? = null
): Pair<GrayF32, GrayF32> {
    val resultX = gradientX ?: createSameShape()
    val resultY = gradientY ?: createSameShape()
    GImageDerivativeOps.gradient(DerivativeType.SOBEL, this, resultX, resultY, BorderType.EXTENDED)
    return Pair(resultX, resultY)
}
