package tomasvolker.kloudpoint

import java.nio.file.Path
import java.nio.file.Paths
import kotlin.system.measureTimeMillis

fun String.toPath(): Path = Paths.get(this)

operator fun Path.div(other: Path): Path = resolve(other)
operator fun Path.div(other: String): Path = resolve(other)

data class Measured<T>(
    val result: T,
    val millis: Long
)

inline fun <T> measured(block: ()->T): Measured<T> {
    var result: T? = null
    val millis = measureTimeMillis {
        result = block()
    }
    return Measured(result as T, millis)
}