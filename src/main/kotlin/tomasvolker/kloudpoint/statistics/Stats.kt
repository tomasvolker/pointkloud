package tomasvolker.kloudpoint.statistics

import tomasvolker.kloudpoint.math.doubleset.length
import tomasvolker.kloudpoint.model.BoundingBox3D
import tomasvolker.kloudpoint.model.CloudObject
import tomasvolker.kloudpoint.model.ObjectCategory
import tomasvolker.kloudpoint.model.computeBounds

data class PointCloudStats(
    val totalPointCount: Int,
    val totalPointRange: BoundingBox3D,
    val typeCount: Int,
    val categories: List<ObjectCategory>,
    val countByType: Map<ObjectCategory, Int>,
    val pointCountByType: Map<ObjectCategory, Int>
) {

    override fun toString() = buildString {

        appendln("""
            Point count: $totalPointCount
            Point x range: ${totalPointRange.xRange.length}
            Point y range: ${totalPointRange.yRange.length}
            Point z range: ${totalPointRange.zRange.length}
            Type count : $typeCount

        """.trimIndent())

        append("Category".padEnd(26) + ",")
        append("Object Count".padEnd(15) + ",")
        appendln("Point Count".padEnd(15))

        for (category in categories) {
            append("${category.toString().padEnd(26)},")
            append("${countByType[category].toString().padEnd(15)},")
            appendln(pointCountByType[category].toString().padEnd(15))
        }

    }

}

fun List<CloudObject>.computeStatistics(): PointCloudStats {

    val totalPointCloud = flatMap { it.pointCloud }

    val byType = groupBy { it.category }

    return PointCloudStats(
        totalPointCount = totalPointCloud.size,
        totalPointRange = totalPointCloud.computeBounds(),
        typeCount = byType.keys.size,
        categories = byType.keys.sorted(),
        countByType = byType.mapValues { it.value.size },
        pointCountByType = byType.mapValues { it.value.sumBy { it.pointCloud.size } }
    )
}