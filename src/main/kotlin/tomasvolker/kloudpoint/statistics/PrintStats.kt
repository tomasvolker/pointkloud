package tomasvolker.kloudpoint.statistics

import tomasvolker.kloudpoint.div
import tomasvolker.kloudpoint.io.loadRueMadame
import tomasvolker.kloudpoint.toPath

fun main() {

    val directory = "data".toPath()
    //val file = directory/"GT_Madame1_2.ply"
    val file = directory/"GT_Madame1_3.ply"

    val rueMadame = loadRueMadame(file.toFile())

    val stats = rueMadame.computeStatistics()

    println(stats)

}