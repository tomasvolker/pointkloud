package tomasvolker.kloudpoint.demo

import boofcv.struct.image.GrayF64
import org.openrndr.*
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.*
import org.openrndr.extras.camera.Debug3D
import org.openrndr.math.Vector3
import org.openrndr.math.transforms.rotateX
import tomasvolker.kloudpoint.algorithms.hernandezmarcotegui.HernandezMarcoteguiSegmenter
import tomasvolker.kloudpoint.algorithms.hernandezmarcotegui.mergeImage
import tomasvolker.kloudpoint.div
import tomasvolker.kloudpoint.image.plus
import tomasvolker.kloudpoint.io.loadRueMadame
import tomasvolker.kloudpoint.measured
import tomasvolker.kloudpoint.model.*
import tomasvolker.kloudpoint.model.primitives.Point
import tomasvolker.kloudpoint.toPath
import tomasvolker.kloudpoint.visualization.*
import tomasvolker.koofcv.constructor.grayF32
import tomasvolker.koofcv.constructor.grayF64
import tomasvolker.koofcv.statistics.max
import tomasvolker.koofcv.statistics.min
import tomasvolker.numeriko.core.primitives.modulo
import tomasvolker.openrndr.math.extensions.FPSDisplay
import tomasvolker.openrndr.math.primitives.f
import java.awt.image.BufferedImage
import java.nio.ByteBuffer
import java.nio.ByteOrder
import kotlin.random.Random

fun main() {

    val directory = "data".toPath()
    val file = directory/"GT_Madame1_2.ply"

    println("Loading dataset")

    val (rueMadame, millis) = measured { loadRueMadame(file.toFile()) }

    println("Data loaded in ${millis / 1000.0} seconds")

    val pointCloud = rueMadame.flatMap { it.pointCloud }

    val segmenter = HernandezMarcoteguiSegmenter(
        pixelSize = 0.1,
        flatZoneDelta = 1.0,
        artifactHeightThreshold = 0.1,
        localMaxHeight = 0.2
    )

    segmenter.segment(pointCloud)

    application(
        configuration = configuration {
            title = "PointKloud"
            width = 1000
            height = 800
            windowResizable = true
        },
        program = DemoProgram(
            groundTruth = rueMadame,
            segmenter = segmenter
        )
    )
}

class DemoProgram(
    val groundTruth: List<CloudObject>,
    val segmenter: HernandezMarcoteguiSegmenter
): Program() {

    val imageBuffer: ImageSurfaceBuffer by lazy {
        ImageSurfaceBuffer(segmenter.image.width, segmenter.image.height, segmenter.pixelSize)
    }

    val pointCloudBuffer: PointCloudBuffer by lazy {
        PointCloudBuffer()
    }

    enum class State {
        RAW,
        TOP_IMAGE,
        FLAT_ZONES,
        FLAT_ZONES_PLANE,
        GROUND_MASK,
        FILLED,
        INVERTED,
        INVERTED_WITH_BORDER,
        INVERTED_FILLED,
        BUMPS,
        HMAX,
        ARTIFACT_SEEDS,
        ARTIFACT_SEEDS_FILTERED,
        WATERSHED,
        SEGMENTED,
        GROUND_TRUTH
    }

    var state: State = State.RAW

    val groundCentroid = groundTruth.first { it.category == ObjectCategory.GROUND }.pointCloud.computeCentroid()

    val flatPlane = grayF64(segmenter.image.width, segmenter.image.height) { x, y -> groundCentroid.z }

    val invertedOffset = groundCentroid.z - segmenter.groundMask.map { segmenter.inverted[it] }.average()

    override fun setup() {

        extend(FPSDisplay())
        extend(Debug3D()) {
            orbitalCamera.panTo(rotateX(-90.0) * groundCentroid.toVector3())
        }

        backgroundColor = ColorRGBa.BLACK

        keyboard.keyDown.listen { onKeyDown(it) }

        changeTo(State.RAW)

    }

    fun onKeyDown(event: KeyEvent) {
        when(event.key) {
            KEY_ESCAPE -> application.exit()
            'X'.toInt() -> changeTo(
                State.values()[(state.ordinal+1) modulo State.values().size]
            )
            'Z'.toInt() -> changeTo(
                State.values()[(state.ordinal-1) modulo State.values().size]
            )
        }
    }

    fun changeTo(state: State) {
        when(state) {
            State.RAW -> writeRaw()
            State.GROUND_TRUTH -> writeGroundTruth()
            State.SEGMENTED -> writeSegmented()
            State.TOP_IMAGE -> writeImage()
            State.FLAT_ZONES -> writeFlatZones()
            State.FLAT_ZONES_PLANE -> writeFlatZonesPlane()
            State.GROUND_MASK -> writeGroundMask()
            State.FILLED -> writeFilled()
            State.INVERTED -> writeInverted()
            State.INVERTED_WITH_BORDER -> writeInvertedWithBorders()
            State.INVERTED_FILLED -> writeInvertedFilled()
            State.BUMPS -> writeBumps()
            State.HMAX -> writeHmax()
            State.ARTIFACT_SEEDS -> writeArtifactSeeds()
            State.ARTIFACT_SEEDS_FILTERED -> writeArtifactSeedsFiltered()
            State.WATERSHED -> writeWatershed()
        }
        this.state = state
    }

    fun writeGroundTruth() {
        pointCloudBuffer.writePoints(
            groundTruth.map {
                it.pointCloud to when(it.category) {
                    ObjectCategory.FACADE -> ColorRGBa.RED
                    ObjectCategory.GROUND -> ColorRGBa.GRAY
                    else -> Random.nextColorRGB()
                }
            }
        )
    }

    fun writeRaw() {
        pointCloudBuffer.writePoints(
            groundTruth.map { it.pointCloud to ColorRGBa.WHITE }
        )
    }

    fun writeSegmented() {
        pointCloudBuffer.writePoints(
            listOf(
                segmenter.result.facade to ColorRGBa.RED,
                segmenter.result.ground to ColorRGBa.GRAY
            ) + segmenter.result.artifacts.map { it to Random.nextColorRGB() }
        )
    }

    fun writeImage() {
        imageBuffer.colorMapper = heatColorMapper(
            segmenter.image.min(),
            segmenter.image.max()
        )
        imageBuffer.write(segmenter.image)
    }

    fun writeFlatZones() {

        val color = segmenter.flatZones.mergeImage(flatPlane.width, flatPlane.height)

        imageBuffer.write(
            segmenter.image,
            color
        )
    }
    fun writeFlatZonesPlane() {

        val color = segmenter.flatZones.mergeImage(flatPlane.width, flatPlane.height)

        imageBuffer.write(
            flatPlane, //segmenter.image,
            color
        )
    }

    fun writeGroundMask() {
        imageBuffer.colorMapper = heatColorMapper(
            segmenter.maskedImage.min(),
            segmenter.maskedImage.max()
        )
        imageBuffer.write(segmenter.maskedImage)
    }

    fun writeFilled() {
        imageBuffer.colorMapper = heatColorMapper(
            segmenter.filled.min(),
            segmenter.filled.max()
        )
        imageBuffer.write(segmenter.filled)
    }

    fun writeInverted() {
        imageBuffer.colorMapper = heatColorMapper(
            invertedOffset + segmenter.inverted.min(),
            invertedOffset + segmenter.inverted.max()
        )
        imageBuffer.write(segmenter.inverted + invertedOffset)
    }

    fun writeInvertedWithBorders() {
        imageBuffer.colorMapper = heatColorMapper(
            invertedOffset + segmenter.invertedWithBorder.min(),
            invertedOffset + segmenter.invertedWithBorder.max()
        )
        imageBuffer.write(segmenter.invertedWithBorder + invertedOffset)
    }

    fun writeInvertedFilled() {
        imageBuffer.colorMapper = heatColorMapper(
            invertedOffset + segmenter.invertedFilled.min(),
            invertedOffset + segmenter.invertedFilled.max()
        )
        imageBuffer.write(segmenter.invertedFilled + invertedOffset)
    }

    fun writeBumps() {
        imageBuffer.colorMapper = heatColorMapper(
            groundCentroid.z + segmenter.bumps.min(),
            groundCentroid.z + segmenter.bumps.max()
        )
        imageBuffer.write(segmenter.bumps + groundCentroid.z)
    }

    fun writeHmax() {
        imageBuffer.colorMapper = heatColorMapper(
            groundCentroid.z + segmenter.hMax.min(),
            groundCentroid.z + segmenter.hMax.max()
        )
        imageBuffer.write(segmenter.hMax + groundCentroid.z)
    }

    fun writeArtifactSeeds() {
        imageBuffer.write(
            segmenter.hMax + groundCentroid.z,
            segmenter.artifactSeeds.mergeImage(flatPlane.width, flatPlane.height)
        )
    }

    fun writeArtifactSeedsFiltered() {
        imageBuffer.write(
            segmenter.hMax + groundCentroid.z,
            segmenter.artifactSeedsFiltered.mergeImage(flatPlane.width, flatPlane.height)
        )
    }

    fun writeWatershed() {

        val visible = BufferedImage(flatPlane.width, flatPlane.height, BufferedImage.TYPE_INT_ARGB)
        val colorMap = mutableMapOf<Int, ColorRGBa>()
        segmenter.artifacts.forEach { point, i ->
            visible[point.x, point.y] = colorMap.getOrPut(i) { Random.nextColorRGB() }
        }

        imageBuffer.write(
            segmenter.bumps + groundCentroid.z,
            visible
        )
    }

    override fun draw() {

        drawer.isolated {

            view *= rotateX(-90.0)

            drawStyle.quality = DrawQuality.QUALITY
            depthWrite = true
            depthTestPass = DepthTestPass.LESS_OR_EQUAL

            when(state) {
                State.RAW,
                State.GROUND_TRUTH,
                State.SEGMENTED -> drawPointCloud()
                State.FLAT_ZONES,
                State.FLAT_ZONES_PLANE,
                State.GROUND_MASK,
                State.FILLED,
                State.INVERTED,
                State.INVERTED_WITH_BORDER,
                State.INVERTED_FILLED,
                State.BUMPS,
                State.HMAX,
                State.ARTIFACT_SEEDS,
                State.ARTIFACT_SEEDS_FILTERED,
                State.WATERSHED,
                State.TOP_IMAGE-> drawImage()
            }


        }


    }

    fun Drawer.drawPointCloud() {
        pointCloudBuffer.draw(this)
    }

    fun Drawer.drawImage() {
        isolated {
            translate(segmenter.projection.delta)
            imageBuffer.draw(this)
        }
    }

}


class ImageSurfaceBuffer(
    val width: Int,
    val height: Int,
    val pixelSize: Double = 1.0,
    var colorMapper: (Double)->ColorRGBa = Random.nextColorRGB().let { color -> { color } }
) {

    val vertexCount get() = width * height
    val triangleCount get() = 2 * (width-1) * (height-1)
    val segmentCount get() = (width-1) * height + width * (height-1)

    val vertexBuffer = vertexBuffer(
        vertexFormat {
            position(3)
            color(4)
        },
        vertexCount
    )

    fun write(image: GrayF64) {
        vertexBuffer.put {

            for (y in 0 until height) {
                for (x in 0 until width) {
                    val point = image[x, y]
                    write(x = (x * pixelSize).f, y = (y * pixelSize).f, z = point.f)
                    write(colorMapper(point))
                }
            }

        }
    }

    fun write(
        heights: GrayF64,
        colors: BufferedImage
    ) {
        vertexBuffer.put {

            for (y in 0 until height) {
                for (x in 0 until width) {
                    val point = heights[x, y]
                    write(x = (x * pixelSize).f, y = (y * pixelSize).f, z = point.f)
                    write(colors[x, y])
                }
            }

        }
    }

    var triangleIndexBuffer: IndexBuffer = indexBuffer(3 * triangleCount, IndexType.INT32).apply {

        val temp = ByteBuffer.allocateDirect(3 * triangleCount * 4).order(ByteOrder.nativeOrder())
        val intTemp = temp.asIntBuffer()

        for (y in 0 until height-1) {
            for (x in 0 until width-1) {

                intTemp.put(toLinear(x, y))
                intTemp.put(toLinear(x+1, y))
                intTemp.put(toLinear(x, y+1))

                intTemp.put(toLinear(x+1, y))
                intTemp.put(toLinear(x+1, y+1))
                intTemp.put(toLinear(x, y+1))

            }
        }

        intTemp.flip()

        write(temp)

    }

    var wireframeIndexBuffer: IndexBuffer = indexBuffer(2 * segmentCount, IndexType.INT32).apply {

        val temp = ByteBuffer.allocateDirect(2 * segmentCount * 4).order(ByteOrder.nativeOrder())
        val intTemp = temp.asIntBuffer()

        for (y in 0 until height-1) {
            for (x in 0 until width) {
                intTemp.put(toLinear(x, y))
                intTemp.put(toLinear(x, y+1))
            }
        }

        for (x in 0 until width-1) {
            for (y in 0 until height) {
                intTemp.put(toLinear(x, y))
                intTemp.put(toLinear(x+1, y))
            }
        }

        intTemp.flip()

        write(temp)

    }

    fun toLinear(x: Int, y: Int) = y * width + x

    fun draw(drawer: Drawer) {

        drawer.isolated {

            shadeStyle = shadeStyle {
                fragmentTransform = """x_fill.rgba = va_color;"""
            }
            drawer.vertexBuffer(triangleIndexBuffer, listOf(vertexBuffer), DrawPrimitive.TRIANGLES)

            val viewDirection = view.inversed * Vector3.UNIT_Z
            translate(viewDirection * 0.01)
            shadeStyle = null
            fill = ColorRGBa.BLACK
            drawer.vertexBuffer(wireframeIndexBuffer, listOf(vertexBuffer), DrawPrimitive.LINES)


        }

    }

}


class PointCloudBuffer() {

    private var vertexBuffer = vertexBuffer(
        vertexFormat {
            position(3)
            color(4)
        },
        1
    )

    private var size: Int = 1

    private fun assureSize(size: Int) {

        if (vertexBuffer.vertexCount < size) {
            vertexBuffer.destroy()
            vertexBuffer = vertexBuffer(
                vertexFormat {
                    position(3)
                    color(4)
                },
                size
            )
        }

    }

    fun writePoints(pointCloud: List<Point>, color: ColorRGBa) =
            writePoints(pointCloud) { color }

    fun writePoints(pointCloud: List<Point>, colorMapper: (Point) -> ColorRGBa) {

        assureSize(pointCloud.size)

        vertexBuffer.put {

            pointCloud.forEach {

                write(it)
                write(colorMapper(it))

            }

        }

        size = pointCloud.size

    }

    fun writePoints(pointCloud: Iterable<Pair<List<Point>, ColorRGBa>>) {

        val totalSize = pointCloud.map { it.first.size }.sum()

        assureSize(totalSize)

        vertexBuffer.put {

            pointCloud.forEach { (points, color) ->

                points.forEach {
                    write(it)
                    write(color)
                }

            }

        }

        size = totalSize

    }

    fun draw(drawer: Drawer) {

        drawer.isolated {
            shadeStyle = shadeStyle {
                fragmentTransform = """x_fill.rgba = va_color;"""
            }
            vertexBuffer(vertexBuffer, DrawPrimitive.POINTS, vertexCount = size)
        }

    }

}