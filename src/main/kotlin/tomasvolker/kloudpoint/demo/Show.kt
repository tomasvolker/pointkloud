package tomasvolker.kloudpoint.demo

import org.openrndr.color.ColorRGBa
import tomasvolker.kloudpoint.algorithms.estimateNormals
import tomasvolker.kloudpoint.div
import tomasvolker.kloudpoint.io.loadRueMadame
import tomasvolker.kloudpoint.math.doubleset.centroid
import tomasvolker.kloudpoint.math.doubleset.closedRange
import tomasvolker.kloudpoint.math.doubleset.length
import tomasvolker.kloudpoint.measured
import tomasvolker.kloudpoint.model.*
import tomasvolker.kloudpoint.model.primitives.Segment
import tomasvolker.kloudpoint.toPath
import tomasvolker.kloudpoint.visualization.model.Plot3D
import tomasvolker.kloudpoint.visualization.model.color
import tomasvolker.kloudpoint.visualization.scatter
import tomasvolker.kloudpoint.visualization.segments
import tomasvolker.kloudpoint.visualization.showPlot3D

fun main() {

    val directory = "data".toPath()
    val file = directory/"GT_Madame1_3.ply"

    val (rueMadame, millis) = measured { loadRueMadame(file.toFile()) }

    println("Loaded in $millis millis")

    val obj = rueMadame.filter { it.category == ObjectCategory.CARS }[3]
    //val obj = rueMadame.filter { it.category == ObjectCategory.FACADE }[2]
    //val obj = rueMadame.filter { it.category == ObjectCategory.MOTORCYCLES }[0]

    obj.printStats()

    println("Estimating Normals")

    val (normals, normalsMillis) = measured {
        obj.pointCloud.estimateNormals(
            radius = 0.2,
            positions = obj.pointCloud
        )
    }

    println("Normals estimation took $normalsMillis millis")

    showPointCloud(obj.pointCloud) {

        title = "Rue madame"

        segments {
            segmentList = normals.map {
                Segment(
                    start = it.point,
                    end = it.point + it.feature * 0.05
                )
            }
            color = ColorRGBa.RED
        }

    }

}

fun showPointCloud(
    pointCloud: PointCloud,
    init: Plot3D.Builder.()->Unit = {}
) {

    showPlot3D {

        scatter(pointCloud = pointCloud) {
            color = ColorRGBa.BLUE
        }

        init()
    }

}

fun BoundingBox3D.maxSide(): BoundingBox3D {

    val rangeList = listOf(xRange, yRange, zRange)
    val midRange = (rangeList.map { it.length }.max() ?: error("")) / 2
    val ranges = rangeList.map { closedRange(it.centroid - midRange, it.centroid + midRange) }

    return BoundingBox3D(ranges)
}