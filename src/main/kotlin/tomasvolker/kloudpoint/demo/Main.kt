package tomasvolker.kloudpoint.demo

import org.openrndr.color.ColorRGBa
import tomasvolker.kloudpoint.algorithms.kdtree.MidRangePolicy
import tomasvolker.kloudpoint.algorithms.kdtree.buildKdTree
import tomasvolker.kloudpoint.algorithms.estimateNormals
import tomasvolker.kloudpoint.algorithms.kdtree.printStats
import tomasvolker.kloudpoint.div
import tomasvolker.kloudpoint.io.loadRueMadame
import tomasvolker.kloudpoint.math.doubleset.max
import tomasvolker.kloudpoint.math.doubleset.min
import tomasvolker.kloudpoint.measured
import tomasvolker.kloudpoint.model.*
import tomasvolker.kloudpoint.model.primitives.Segment
import tomasvolker.kloudpoint.statistics.computeStatistics
import tomasvolker.kloudpoint.toPath
import tomasvolker.kloudpoint.visualization.model.CameraType
import tomasvolker.kloudpoint.visualization.model.color
import tomasvolker.kloudpoint.visualization.nextColorRGB
import tomasvolker.kloudpoint.visualization.scatter
import tomasvolker.kloudpoint.visualization.segments
import tomasvolker.kloudpoint.visualization.showPlot3D
import kotlin.random.Random

fun main() {

    val directory = "data".toPath()
    val file = directory/"GT_Madame1_3.ply"

    val (rueMadame, millis) = measured { loadRueMadame(file.toFile()) }

    println("Time: $millis")

    val stats = rueMadame.computeStatistics()

    println(stats)

    val colorMap = ObjectCategory.values().map { it to Random.nextColorRGB() }.toMap()

    val cars = rueMadame.filter { it.category == ObjectCategory.CARS }

    cars.forEachIndexed { i, it -> println("${i}: ${it.pointCloud.size}") }

    val car = cars[10]

    car.printStats()

    val pointList = car.pointCloud

    val kdtree = pointList.buildKdTree(partitionPolicy = MidRangePolicy(maxSize = 100))

    kdtree.printStats()

    val normals = pointList.estimateNormals(
        positions = pointList,
        radius = 0.2,
        tree = kdtree
    )
/*
    showPlot {

        histogram {
            data = normals.map { it.feature.x }
            bins = 100
            color = tomasvolker.kyplot.model.Color.RED
            alpha = 0.5
            label = "x"
        }

        histogram {
            data = normals.map { it.feature.y }
            bins = 100
            color = tomasvolker.kyplot.model.Color.GREEN
            alpha = 0.5
            label = "y"
        }

        histogram {
            data = normals.map { it.feature.z }
            bins = 100
            color = tomasvolker.kyplot.model.Color.BLUE
            alpha = 0.5
            label = "z"
        }

        legend.visible = true
    }
*/
    /*
    val zRange = rueMadame.fold(emptyDoubleSet) { acc, cloudObject ->
        acc union cloudObject.pointCloud.computeBounds().zRange
    }
    */
    val zRange = pointList.computeBounds().zRange

    println("zRange: $zRange")

    val min = zRange.min
    val max = zRange.max

    fun equalizer(x: Double) = (x - min) / (max - min)

    showPlot3D {

        title = "Rue madame"

        camera = CameraType.ORBITAL

        for (obj in rueMadame) {
            scatter(pointCloud = obj.pointCloud) {
                //color = colorMap[obj.category]
                color = Random.nextColorRGB()
                //colorMapper = { point -> equalizer(point.z).let { ColorRGBa(it, it, it) } }
            }
        }

        initialPosition = car.pointCloud.computeCentroid()
/*
        segments {
            segmentList = normals.map {
                Segment(
                    start = it.point,
                    end = it.point + it.feature / 10
                )
            }
            color = ColorRGBa.RED
        }
*/
    }

}
